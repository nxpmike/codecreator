// CodeCreator RB29AUg13 rev08
// ===========
// Bugs:
//   - crash when clicking STEP 1: was maxbtns=30 not enough?
//   - selected module seems to be wrong number +/-1
//   - fix all conditions
//   - sometimes mouse is offset in y-axis
//   - sometimes takes a while to load main screen
//
//   rev08: changed "return 0;" to "// return 0;"
//
//-------------------------------------------------------------------
import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.security.CodeSource;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
//===================================================================
public class CodeCreator implements MouseListener, MouseMotionListener
{
private RGUI rgui;
private JFrame frame;
private final int maxmcus=50;
private final int maxmodules=50;
private final int maxelements=10;
private int nmcus,nmodules;
private String fMCU[];
private String sMCU[];
private String sModule[];
private String sModUses[];
private String sElements[][];
private int xt,yt,xm,ym,hover,mcusel;
private String scode;
private URL jar;
/*
private int Nevks,Nbuts,Neths,Ndsps,Nargs,Nouts;
private final int maxevks=12;
private final int maxbuts=6;
private final int maxeths=3;
private final int maxdsps=10;
private final int maxargs=500;
private final int maxouts=50;
private final int maxvars=26;
private String sevk[],sbut[],seth[],sdsp[],u[],sout[],svar[];
private char cevk[],cbut[],ceth[],cdsp[];
//,nbtns,sel;
private char q0,q1,q2,q3;
*/
//-------------------------------------------------------------------
public static void main(String[] args) {CodeCreator gui=new CodeCreator(); gui.InitAll();}
//-------------------------------------------------------------------  
public void InitAll()
{
rgui=new RGUI();
fMCU=new String[maxmcus];
sMCU=new String[maxmcus];
sModule=new String[maxmodules];
sModUses=new String[maxmodules];
sElements=new String[maxmodules][maxelements];
frame=new JFrame("CodeCreator - v1.08 - Rod Borras/Mike Steffen");
frame.getContentPane().add(BorderLayout.CENTER,rgui);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setSize(1024,768);
frame.setVisible(true);
frame.addMouseListener(this);
frame.addMouseMotionListener(this);
rgui.addbutton( 10, 10,300, 30,1,10,"Step 1: Pick your MCU");
rgui.addbutton(350, 10,300, 30,0,10,"Step 2: Pick your Module");
rgui.addbutton(700, 10,300, 30,0,10,"Step 3: Generate Code");
ListMCUs();
frame.repaint();
}
//-------------------------------------------------------------------  
public void ListMCUs()
{
String line,stemp;
int i,j,l,i1,i2,i3,i4;
boolean notdone;
String ftemp;
File dir;
nmcus=0; rgui.btns=3; rgui.step=1;
CodeSource src=CodeCreator.class.getProtectionDomain().getCodeSource();
rgui.clearcomments();
try
  {
  if (src!=null) 
    {
    jar=src.getLocation(); System.out.println(jar);
    ZipInputStream zip=new ZipInputStream(jar.openStream());
    ZipEntry ze=null;
    while ((ze=zip.getNextEntry())!=null) 
      {
      String entryName=ze.getName(); //System.out.println(entryName);
      if (entryName.endsWith(".txt")) 
        {
	Reader fileReader=new InputStreamReader(CodeCreator.class.getClassLoader().getResourceAsStream(entryName)); 
        BufferedReader reader=new BufferedReader(fileReader);
        notdone=true;
	while (notdone) 
	  {
	  line=reader.readLine(); if (line==null) notdone=false;
	  if (notdone)
	    {
	    l=line.length();
	    if ((l>10)&&(line.substring(0,8).equals("MenuName")))
	      {sMCU[nmcus]=line.substring(9,l); fMCU[nmcus]=entryName;}
            if ((l>10)&&(line.substring(0,11).equals("MenuComment")))
	      rgui.sComment[nmcus]=line.substring(12,l);
	    }
	  else {reader.close(); fileReader.close();}
	  }//while (notdone)
	nmcus++;
        }//if (entryName)
      }//while
    }//if
  }//try
catch (Exception ex) {System.out.println("Error #01: Could not extract MCU files.");}
// Sort Results
for (i=0;i<nmcus-1;i++) for (j=i;j<nmcus;j++)
  if (sMCU[i].compareTo(sMCU[j])>0) 
    {
    ftemp=fMCU[i]; fMCU[i]=fMCU[j]; fMCU[j]=ftemp; 
    stemp=sMCU[i]; sMCU[i]=sMCU[j]; sMCU[j]=stemp;
    stemp=rgui.sComment[i]; rgui.sComment[i]=rgui.sComment[j]; rgui.sComment[j]=stemp;
    }
// Create Buttons
i1=i2=i3=i4=0;
System.out.print("N MCUs="); System.out.println(nmcus); 
for (i=0;i<nmcus;i++) 
  {
       if (sMCU[i].startsWith("RS08")) {rgui.addbutton( 20+170*(i1/11),90+50*(i1%11),150, 30,1,10,sMCU[i]); i1++;}
  else if (sMCU[i].startsWith("S08" )) {rgui.addbutton(200+170*(i2/11),90+50*(i2%11),150, 30,1,10,sMCU[i]); i2++;}
  else if (sMCU[i].startsWith("M0+" )) {rgui.addbutton(550+170*(i3/11),90+50*(i3%11),150, 30,1,10,sMCU[i]); i3++;}
  else if (sMCU[i].startsWith("M4"  )) {rgui.addbutton(730+170*(i4/11),90+50*(i4%11),150, 30,1,10,sMCU[i]); i4++;}
  }
//for (i=0;i<nmcus;i++) rgui.addbutton(50+170*(i/12),50+50*(i%12),150, 30,1,10,sMCU[i]);
}
//-------------------------------------------------------------------  
public void LoadMCU(int mcu)
{
String line,smodname;
int i,j,l,state,ee,element;
boolean notdone;
char c;
nmodules=0; rgui.btns=3; rgui.step=2; state=element=0;
for (i=0;i<maxmodules;i++) sModUses[i]="";
for (i=0;i<maxmodules;i++) for (j=0;j<maxelements;j++) sElements[i][j]="";
rgui.clearcomments();
try
  {
  Reader fileReader=new InputStreamReader(CodeCreator.class.getClassLoader().getResourceAsStream(fMCU[mcu])); 
  BufferedReader reader=new BufferedReader(fileReader);
  notdone=true;
  while (notdone) 
    {
    line=reader.readLine(); if (line==null) notdone=false;
    if (notdone)
      {
      l=line.length();
      ee=0; 
      if (l>0) {ee=line.charAt(0); if ((ee>'0')&&(ee<'9')) ee-='0'; else ee=0;}
           if ((l> 8)&&(line.substring(0, 6).equals("MODULE")))    state=1;
      else if ((l> 8)&&(line.substring(0, 9).equals("ENDMODULE"))) state=4;
      else if ((l>15)&&(line.substring(0,13).equals("ModuleComment"))) rgui.sComment[nmodules]=line.substring(14,l);
      else if ((l>2)&&(line.charAt(1)=='.')&&(ee!=0)) state=2;
      switch (state)
        {
        case 0: // Nothing happening
          break;
        case 1: // Beginning of Module
          smodname=""; i=7;
          do
            {
            c=line.charAt(i);
            if (((c>='0')&&(c<='Z'))||(c=='_')||(c=='/')) {smodname+=c; i++;} else i=500;
            }
          while (i<l);
          sModule[nmodules]=smodname;
          i=line.indexOf("uses"); if (i>0) sModUses[nmodules]=line.substring(i+5,l);
          state=0;
          break;
        case 2: // Module Element Header
          element=ee;
          state=3;
          break;
        case 3: // Copy Module Elements
          sElements[nmodules][element]=sElements[nmodules][element]+line+"\n";
          /*
          if ((build)&&(mModule[mm].GetCheck()==BST_CHECKED))
            {
            if ((smodname=="INIT")&&(element==6))
              InitMain[InitMainC++]=sline;
            else if (element!=6)
              {
              Elements[element-1][ElementCount[element-1]]=sline;
              (ElementCount[element-1])++;
              }
            else if (ModCheck[mm])  // only add main if truly checked
              {
              Elements[element-1][ElementCount[element-1]]=sline;
              (ElementCount[element-1])++;
              }
            }
          */
          break;
        case 4: // End of Module
          nmodules++;
          state=0;
          break;
        }//switch

/*
      if ((l>8)&&(line.substring(0,6).equals("MODULE")))
        {sModule[nmodules]=line.substring(7,l); nmodules++;}
      if ((l>15)&&(line.substring(0,13).equals("ModuleComment")))
        rgui.sComment[nmodules-1]=line.substring(14,l);
*/


      }
    else {reader.close(); fileReader.close();}
    }//while
  }//try
catch (Exception ex) {System.out.println("Error #02: Could not extract MCU Modules.");}
//Create Buttons
//System.out.println(nmodules);
for (i=0;i<nmodules;i++) rgui.addbutton(50+280*(i/12),50+50*(i%12),250, 30,1,10,sModule[i]); 
}
//-------------------------------------------------------------------  
public void LoadModule(int module)
{
rgui.btns=3; rgui.step=3;
CreateMain(module);
ShowMain();
CreateMainFile();
/*
rgui.addbutton(300,200,400, 60,1,10,"Create main.c for IAR");
rgui.addbutton(300,400,400, 60,1,10,"Create main.c for CodeWarrior");
rgui.sComment[0]="This will create a main.c file specific to IAR./It will also create a popup window to allow you to extract a section./";
rgui.sComment[1]="This will create a main.c file specific to CodeWarrior./It will also create a popup window to allow you to extract a section./";
*/
}
//-------------------------------------------------------------------  
public void AddElements(int addmodule,int addelement)
{
int imod;
String sdbg=String.format("%d",addmodule);
sdbg+="***"+sModUses[addmodule]+"--->"+sModule[addmodule]; System.out.println(sdbg);
if (addelement==6) scode+="int main(void)                                                                           \n{\n";
for (imod=0;imod<nmodules;imod++) 
  if (    ( (addelement==6) && (imod==addmodule) )
       || ( (addelement!=6) && ((imod==0) || (imod==addmodule)) )  
       || ( (addelement!=6) && (sModUses[addmodule].length()>0) && (sModUses[addmodule].indexOf(sModule[imod])>0) )  )
    scode+=sElements[imod][addelement];
if (addelement==6) 
  {
  scode+="// return 0;                                                                             \n";
  scode+="}                                                                                        \n";
  scode+="//-------------------------------------------------------------------                    \n";
  scode+="// ******************************************************************                    \n";
  }
scode+="\n\n";
}
//-------------------------------------------------------------------  
public void CreateMain(int module)
{
int j;
scode="";
scode+="// INCLUDES /////////////////////////////////////////////////////////                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,1);
scode+="// DEFINES //////////////////////////////////////////////////////////                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,2);
scode+="// GLOBAL CONSTANTS /////////////////////////////////////////////////                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,3);
scode+="// GLOBAL VARIABLES /////////////////////////////////////////////////                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,4);
scode+="// FUNCTION HEADERS /////////////////////////////////////////////////                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,5);
scode+="// *** MAIN *********************************************************                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,6);
scode+="// FUNCTION BODIES //////////////////////////////////////////////////                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,7);
scode+="// INTERRUPT BODIES /////////////////////////////////////////////////                    \n";
scode+="//-------------------------------------------------------------------                    \n";
AddElements(module,8);
//for (j=0;j<maxelements;j++) scode+=sElements[module][j]+"\n";
}
//-------------------------------------------------------------------  
public void ShowMain()
{
JFrame framecode;
JTextArea rodcode;
JScrollPane spane;
int j;
Font fontcode=new Font("Courier New",Font.BOLD,15);
rodcode=new JTextArea(200,200);
rodcode.setText(scode);
rodcode.setFont(fontcode);
spane=new JScrollPane(rodcode);
framecode=new JFrame("main.c");
//frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
framecode.setSize(1000,580);
framecode.setLocation(75,200);
framecode.setVisible(true);  
framecode.add(spane);
}
//-------------------------------------------------------------------  
public void CreateMainFile()
{
String sfile;
sfile=jar+"";
sfile=sfile.substring(5,sfile.length()-6)+"main.c";  //    /home/rod/code/work/java/cc04/classes/main.c
System.out.println(sfile);
try
 {
 File myFile=new File(sfile);
 FileWriter fileWriter=new FileWriter(myFile);
 fileWriter.write(scode);
 fileWriter.close();
 }
catch (Exception ex) {System.out.println("Could not create result file.");}
}
//-------------------------------------------------------------------  
public void ManageMouse(MouseEvent e,int motion)
{
int rodsel;
String s;
xt=e.getX();
yt=e.getY()-30;
rodsel=rgui.testbuttons(xt,yt,motion);
s=String.format("xt=%3d yt=%3d  mtn=%3d   rodsel=%3d",xt,yt,motion,rodsel); System.out.println(s);
if ((rodsel>=0)&&(motion==1))
  {
  //..........Handle a click of the top 3 buttons....................
  if (rodsel==0)
    {
    // Step 1: Pick your MCU
    rgui.showbutton(0,1); rgui.showbutton(1,0); rgui.showbutton(2,0); ListMCUs();
    }
  else if (rodsel==1)
    {
    // Step 2: Pick your Modules
    rgui.showbutton(0,1); rgui.showbutton(1,1); rgui.showbutton(2,0); LoadMCU(mcusel);
    }
  else if (rodsel==2)
    {
    // Step 3: Generate Code
    System.out.println("Step 3");
    }
  //..........Handle a click of the MCU or Module buttons............
  else if (rgui.step==1)
    {
    rgui.showbutton(1,1); mcusel=rodsel-3; LoadMCU(mcusel); System.out.print("1SEL="); System.out.println(rodsel);
    }
  else if (rgui.step==2)
    {
    rgui.showbutton(1,1); rgui.showbutton(2,1); LoadModule(rodsel-3); System.out.print("2SEL="); System.out.println(rodsel);
    }
  else if (rgui.step==3)
    {
    rgui.showbutton(1,1); rgui.showbutton(2,1); CreateMain(rodsel-3); System.out.print("3SEL="); System.out.println(rodsel);
    }
  }//if
//rgui.invalidate();
frame.repaint();
}
//-------------------------------------------------------------------  
public void mousePressed(MouseEvent e)  {ManageMouse(e,2);}
public void mouseReleased(MouseEvent e) {ManageMouse(e,1);} // UpdateArgs();}
public void mouseEntered(MouseEvent e)  {System.out.println("enter");}
public void mouseExited(MouseEvent e)   {}
public void mouseClicked(MouseEvent e)  {}
//...................................................................
public void mouseDragged(MouseEvent e)  {}
public void mouseMoved(MouseEvent e)
{
xm=e.getX();
//ym=e.getY()-30;
ym=e.getY()-30;
hover=rgui.testbuttons(xm,ym,1);
rgui.ncomment=hover-3;
frame.repaint();
}
//===================================================================
}//class CodeCreator
//===================================================================


