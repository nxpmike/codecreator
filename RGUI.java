//-------------------------------------------------------------------
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;
//===================================================================
public class RGUI extends JPanel
{
public int sel,mtn,btns,step,ncomment;
public String sComment[];
//...................................................................
private final int maxbtns=50;
private int B[][];
private String BS[];
private final int maxcomments=50;
private Color colbkgd,colfram,coltext;
/////////////////////////////////////////////////////////////////////
private static final long serialVersionUID=314159L;
private final int x0=15,y0=25,xw0=300,dx=250,dy=40;
//-------------------------------------------------------------------
public RGUI()
{
int i;
btns=0;
B=new int[maxbtns][9];
BS=new String[maxbtns];
sComment=new String[maxcomments];
for (i=0;i<maxcomments;i++) sComment[i]="???";
}
//-------------------------------------------------------------------
public void addbutton(int bx0,int by0,int bdx,int bdy,int bshow,int btxtsiz,String btext)
{
B[btns][0]=bx0; B[btns][1]=by0; B[btns][2]=bdx; B[btns][3]=bdy; 
B[btns][4]=bshow; B[btns][5]=btxtsiz; BS[btns]=btext; 
btns++;
}
//-------------------------------------------------------------------
public void renamebutton(int bn,String btext) {BS[bn]=btext;}
//-------------------------------------------------------------------
public void showbutton(int bn,int bshow) {B[bn][4]=bshow;}
//-------------------------------------------------------------------
public int testbuttons(int bxx,int byy,int bmotion)
{
int ti;
ti=0; sel=-1; if (bmotion!=0) mtn=bmotion;
while ((ti<btns)&&(sel==-1)) 
  {if ((bxx>B[ti][0])&&(bxx<B[ti][0]+B[ti][2])&&(byy>B[ti][1])&&(byy<B[ti][1]+B[ti][3])&&(B[ti][4]!=0)) sel=ti; else ti++;}
return sel;
}
//-------------------------------------------------------------------
public void clearcomments()
{
int ci;
for (ci=0;ci<maxcomments;ci++) sComment[ci]="";
}
//-------------------------------------------------------------------
public void paintComponent(Graphics g)
{
int bi,c0,c1,xa,ya,dx,dy;
String s;
Font font0=new Font("Arial",Font.BOLD,20);
Font font1=new Font("Arial",Font.BOLD,15);
Font font2=new Font("Courier",Font.BOLD,10);
g.setFont(font0);
g.setColor(Color.white);
// If Pick MCU, draw MCU Family Boxes
if (step==1)
  {
  xa=10; ya=50; dx=170; dy=590; 
  g.setColor(Color.black); g.fillRect(xa  ,ya  ,dx   ,dy   ); 
  g.setColor(Color.white); g.fillRect(xa+5,ya+5,dx-10,dy-10);
  g.setColor(Color.black); g.drawString("RS08",xa+10,ya+25);
  xa=190; ya=50; dx=340; dy=590; 
  g.setColor(Color.black); g.fillRect(xa  ,ya  ,dx   ,dy   ); 
  g.setColor(Color.white); g.fillRect(xa+5,ya+5,dx-10,dy-10);
  g.setColor(Color.black); g.drawString("S08",xa+10,ya+25);
  xa=540; ya=50; dx=170; dy=590; 
  g.setColor(Color.black); g.fillRect(xa  ,ya  ,dx   ,dy   ); 
  g.setColor(Color.white); g.fillRect(xa+5,ya+5,dx-10,dy-10);
  g.setColor(Color.black); g.drawString("ARM M0+",xa+10,ya+25);
  xa=720; ya=50; dx=170; dy=590; 
  g.setColor(Color.black); g.fillRect(xa  ,ya  ,dx   ,dy   ); 
  g.setColor(Color.white); g.fillRect(xa+5,ya+5,dx-10,dy-10);
  g.setColor(Color.black); g.drawString("ARM M4",xa+10,ya+25);
  }
// Draw Menu Buttons
for (bi=0;bi<btns;bi++)
  {
  if (B[bi][4]!=0)
	  {
	  //g.setTextSize(B[bi][5]);
	       if ((bi==step-1)&&(mtn!=2)) {colbkgd=Color.BLUE; colfram=Color.BLUE;  coltext=Color.BLACK;}
	  else if ((sel==bi)   &&(mtn==1)) {colbkgd=Color.GREEN; colfram=Color.RED;   coltext=Color.BLUE;}
	  else if ((sel==bi)   &&(mtn==2)) {colbkgd=Color.GREEN; colfram=Color.BLUE;  coltext=Color.BLACK;}
    else                             {colbkgd=Color.WHITE; colfram=Color.BLACK; coltext=Color.BLUE;} 
    //
    g.setColor(colbkgd);
    g.fillRoundRect(B[bi][0],B[bi][1],B[bi][2],B[bi][3],20,20);
    g.setColor(colfram); 
    g.drawRoundRect(B[bi][0],B[bi][1],B[bi][2],B[bi][3],20,20); 
    g.setColor(coltext); 
    //paint.setStyle(style1); paint.setStrokeWidth(fx(width1));
    g.drawString(BS[bi],(int)(B[bi][0]+B[bi][2]*0.03f),(int)(B[bi][1]+B[bi][3]*0.8f));
    }//if
  }//for
// Show "Hover Comments"
if (ncomment>=0)
	{
  g.setColor(Color.RED);
  s=sComment[ncomment];
  for (c0=0;c0<4;c0++)
  	{
    c1=s.indexOf('/');
    if (c1>0) {g.drawString(s.substring(0,c1),50,660+22*c0); s=s.substring(c1+1,s.length());}
  	}
	}
/*

g.fillRoundRect(B[i][1],B[i][2],B[i][3],B[i][4],B[i][5],B[i][6]);
g.setColor(Color.blue);    g.drawString("i.MX Board", x0+0*xw0,y0-5); g.fillRect(x0+0*xw0,y0 ,dx+10,550);
 g.setColor(Color.magenta); g.drawString("Boot Method",x0+1*xw0,y0-5); g.fillRect(x0+1*xw0,y0 ,dx+10,280);
 g.setColor(Color.darkGray);g.drawString("Ethernet",   x0+1*xw0,335);  g.fillRect(x0+1*xw0,340,dx+10,150);
 g.setColor(Color.orange);  g.drawString("Display",    x0+2*xw0,y0-5); g.fillRect(x0+2*xw0,y0 ,dx+10,465);
 g.setFont(font1);
 // Show Menu
 for (i=0;i<nbtns;i++) 
   {
   if (B[i][8]!=0)
     {
     switch(B[i][7])
       {
       case 0: g.setColor(Color.black); break;
       case 1: g.setColor(Color.black); break;
       case 2: g.setColor(Color.red);   break;
       }
     g.fillRoundRect(B[i][1],B[i][2],B[i][3],B[i][4],B[i][5],B[i][6]); 
     switch(B[i][7])
       {
       case 0: g.setColor(Color.white); break;
       case 1: g.setColor(Color.green); break;
       case 2: g.setColor(Color.green); break;
       }
     g.fillRect(B[i][1]+10,B[i][2]+10,B[i][3]-20,B[i][4]-20);
     g.setColor(Color.black);
     g.drawString(BS[i],B[i][1]+15,B[i][2]+25);
     }
   //s=String.format("X=%3d  Y=%3d  sel=%2d  mtn=%d nbtns=%d",xt,yt,sel,mtn,nbtns); g.drawString(s,300,700);
 //s=String.format("%3d: %3d %3d %3d %3d %3d",i,B[i][0],B[i][1],B[i][2],B[i][3],B[i][4]); g.drawString(s,400,50*i+50);
   }
 // Show Result
 g.setFont(font2);
 //for (i=0;i<Nouts;i++) g.drawString(sout[i],10,10*i+600);
 //for (i=0;i<maxvars;i++) g.drawString(svar[i],800,10*i+600);
 // Show Status
 //s=String.format("Nargs=%d  q1=[%c]  q2=[%c%c]  q3=[%c%c]  q4=[%c%c]",Nargs,q1,q2a,q2b,q3a,q3b,q4a,q4b); g.drawString(s,350,550);
 //s=String.format("[%c%c%c%c]  Nevks=%d  Nbuts=%d  Neths=%d  Ndsps=%d  nbtns=%d  Nargs=%d",q0,q1,q2,q3,Nevks,Nbuts,Neths,Ndsps,nbtns,Nargs); 
 //g.drawString(s,320,550);
  */
} 
//-------------------------------------------------------------------
}//class RGUI
//===================================================================
