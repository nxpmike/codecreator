Revision=4
MenuName=RS08_KB2
MenuComment=8-bit/RS08 Core/2KB Flash/63B RAM/
IncludeName=<MC9RS08KB2.h>
MCUID=0x009
CURRENTDEVICE=9RS08KB2


MODULE:INIT
1. Includes
#include "derivative.h"                      // gives acces to all MCU registers
2. Defines
#define Overflow1    MTIM1SC_TOF             // used by MCU_Delay()
#define LEDR         PTAD_PTAD2              // Red LED, positive logic (1=on, 0=off)
#define nSW4         PTAD_PTAD3              // Switch 4,  negative logic (0=pressed, 1=not pressed)
#define Pot          0x01                    // Potentiometer is on A/D channel 0x01
3. Global Constants
4. Global Variables
byte adcin;                                  // adcin will store ADC results
5. Function Headers
void MCU_Init(void);                         // initializes MCU for the Universal S08 Tower Board
void MCU_Delay(word delay);                  // delay in multiples of 100us
//---------------------------------------------------------------------
void Buzzer(byte OnOff);                     // Buzzer Control:  1=on, 0=off
byte ADC_Read(byte chan);                    // Analog-to-Digital Converter byte read: enter channel to read
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
for (;;)                                     // forever loop
  {
  }
7. Function Bodies
void MCU_Init(void) 
{
// Crucial            
SOPT1=0x23;                                  // disable COP, enable Stop mode; Reset & Bkgd = BDM
SPMSC1=0x00;                                 // disable LVDSE
// System Clock Init
ICSTRM=NVICSTRM;                             // Initialize ICSTRM register from NV_ICSTRM
ICSSC =NVFTRIM;                              // Initialize ICSSC  register from NV_FTRIM
ICSC1=0x04;
ICSC2=0x00;                                  // go to full bus speed /1, instead of /2 (default); Trim*512=31250Hz*512=16.000MHz (8.000MHz bus)
// ADC Init
ADCCFG=0x00;                                 // 8 bits & 8MHz clock (/1)
// Modulo Timer Init
MTIM1CLK=0x03;                               // busclk/8=1us per count
MTIM1MOD=100;                                // period=100*1us =100us
MTIM1SC=0x20;                                // start clock
// Timer Init (Buzzer)
TPMSC=0x00;                                  // busclk/1=125ns per count; buzzer on=0x08, off=0x00
TPMC0SC=0x28;                                // edge-aligned PWM
TPMMOD=18182-1;                              // 440Hz by default
TPMC0V=18182>>1;                             // half of the above to produce 50% duty cycle PWM
// GPIO Init
PTAD=0x00;                                   // LEDR (PTA2) off by default                                   
PTADD=0x04;                                  // LEDR (PTA2) is an output
PTAPE=0x08;                                  // enable pull-ups for nSW4 (PTA3)
}
//---------------------------------------------------------------------
void MCU_Delay(word delay)                   // Delay in multiples of 100us (e.g. use 10000 for 1 second)
{word delw; for (delw=0;delw<delay;delw++) {while (!Overflow1); Overflow1=0;}}
//---------------------------------------------------------------------
void Buzzer(byte OnOff)                      // Buzzer Control:  1=on, 0=off
{if (OnOff==1) TPMSC=0x08; else TPMSC=0x00;}
//---------------------------------------------------------------------
byte ADC_Read(byte chan)                     // Analog-to-Digital Converter byte read: enter channel to read
{ADCSC1=chan; while (!ADCSC1_COCO); return ADCRL;}
8. Interrupt Bodies
ENDMODULE


MODULE:SCI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte cin;
5. Function Headers
//---------------------------------------------------------------------
void SCI_Init(void);                         // initializes the serial port
void SCI_CharOut(byte data);                 // sends out a character
void SCI_NibbOut(byte data);                 // sends out a nibble (hex value)
void SCI_ByteOut(byte data);                 // sends out a byte (hex value)
void SCI_TextOut(byte *data);                // sends out a string
byte SCI_CharIn(void);                       // reads a character
byte SCI_ByteIn(void);                       // reads two characters and interprets them as a byte (hex)
void SCI_CRLF(void);                         // send a carriage return + line feed
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
SCI_Init();                                  // also initialize the SCI  
for(;;)                                      // forever loop 
  {
  } 
7. Function Bodies


// SCI FUNCTIONS //////////////////////////////////////////////////////
//---------------------------------------------------------------------
void SCI_Init(void)          {SCIBD=13; SCIC1=0x00; SCIC2=0x0C; SCIS2=0x00; SCIC3=0x00;}   // 8MHz/16/13=38,400 baud (actually 38,462)
//---------------------------------------------------------------------
void SCI_CharOut(byte data)  {while (!SCIS1_TDRE); SCID=data;}
//---------------------------------------------------------------------
void SCI_NibbOut(byte data)  {if (data>0x09) SCI_CharOut(data+0x37); else SCI_CharOut(data+0x30);}
//---------------------------------------------------------------------
void SCI_ByteOut(byte data)  {SCI_NibbOut(data>>4); SCI_NibbOut(data&0x0F);}
//---------------------------------------------------------------------
void SCI_TextOut(byte *data) {while (*data!=0x00) {SCI_CharOut(*data); data++;}}
//---------------------------------------------------------------------
byte SCI_CharIn(void)        {while (!SCIS1_RDRF); return SCID;}
//---------------------------------------------------------------------
byte SCI_ByteIn(void)        {byte h,l; h=SCI_CharIn()-48; if (h>9) h-=7; l=SCI_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
//---------------------------------------------------------------------
void SCI_CRLF(void)          {SCI_CharOut(0x0D); SCI_CharOut(0x0A);}
8. Interrupt Bodies
9. NO SCI support for this module
ENDMODULE


MODULE:IIC uses SCI
1. Includes
2. Defines
#define IIC_SDA      PTAD_PTAD2              // change to match your GPIO
#define IIC_SCL      PTAD_PTAD3              // change to match your GPIO
#define IIC_DD_SDA   PTADD_PTADD2            // change to match your GPIO
#define IIC_DD_SCL   PTADD_PTADD3            // change to match your GPIO
3. Global Constants
4. Global Variables
byte slave,reg,val,cmd;
word found;
5. Function Headers
//---------------------------------------------------------------------
byte IIC_Init(void);
word IIC_Find(byte LastAddress);
byte IIC_SlaveRegRead(byte SlaveAddress,byte Reg,byte *Result);
byte IIC_SlaveRegWrite(byte SlaveAddress,byte Reg,byte Val);
byte IIC_SlaveRegReadN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayIn);
byte IIC_SlaveRegWriteN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayOut);
6. Main
MCU_Init();
SCI_Init();
(void)IIC_Init();
slave=0x01;
for(;;)
  {
  }
7. Function Bodies


// IIC FUNCTIONS (MASTER) /////////////////////////////////////////////
//---------------------------------------------------------------------
//.... Private Functions, hidden from main() ..........................
void Slow()      {byte burn; for (burn=0;burn<20;burn++);}
//.....................................................................
void SDA0()      {IIC_SDA=0; IIC_DD_SDA=1; Slow();}
//.....................................................................
void SDA1()      {           IIC_DD_SDA=0; Slow();}
//.....................................................................
void SCL0()      {IIC_SCL=0; IIC_DD_SCL=1; Slow();}
//.....................................................................
void SCL1()      {           IIC_DD_SCL=0; Slow();}
//.....................................................................
void Ack()       {SDA0(); SCL1(); SCL0();}
//.....................................................................
void Nak()       {SDA1(); SCL1(); SCL0();}
//.....................................................................
void IIC_Start() {SDA1(); SCL1(); SDA0(); SCL0();}
//.....................................................................
void IIC_Stop()  {SDA0(); SCL1(); SDA1();}
//.....................................................................
byte IIC_TxCycle(byte bout) 
{
byte i,p;
word timeout;
timeout=0;
p=0x80;
for (i=0;i<8;i++) 
  {
  if (p&bout) SDA1(); else SDA0();
  SCL1(); SCL0();
  p>>=1;
  }
SDA1();
SCL1();
Slow();
while ((IIC_SDA)&&(timeout<0x0100)) timeout++;
SCL0();
SDA1();
Slow();
if (timeout>=0x0100) return 1; else return 0;
}
//.....................................................................
byte IIC_RxCycle(byte ack) 
{
byte i,p,r;
p=0x80;
r=0;
SDA1();
Slow();
for (i=0;i<8;i++) 
  {
  SCL1();
  Slow();
  if (IIC_SDA) r+=p;
  SCL0();
  Slow();
  p>>=1;
  }
SDA1();
if (ack) Ack(); else Nak();
return r;
} 
//---- Public Functions, seen by main() -------------------------------
byte IIC_Init(void)
{
IIC_DD_SDA=0; IIC_DD_SCL=0; IIC_SDA=0; IIC_SCL=0; Slow();
if ((IIC_SDA)&&(IIC_SCL)) return 0; else return 1;
}
//---------------------------------------------------------------------
word IIC_Find(byte LastAddress)              // tries addresses from 0x00 through LastAddress (clamped to 0x7F)
{                                            // returns word; hi-byte indicates how many slaves were found; lo-byte shows address of last one found
byte found,try,slaveaddress,dummy;
(void)IIC_Init();
slaveaddress=0xFF; dummy=0; found=0;
for (try=0x00;try<=(LastAddress&0x7F);try++) if (IIC_SlaveRegRead(try,0,&dummy)==0) {slaveaddress=try; found++;}
return found<<8|slaveaddress;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegRead(byte SlaveAddress,byte Reg,byte *Result)
{
byte err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(Reg);
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1|1);
*Result=IIC_RxCycle(0);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegWrite(byte SlaveAddress,byte Reg,byte Val)
{
byte err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(Reg);
err|=IIC_TxCycle(Val);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegReadN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayIn)
{
byte i,err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(FirstReg);
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1|1);
for (i=0;i<Nbytes-1;i++) {*ArrayIn=IIC_RxCycle(1); ArrayIn++;}
*ArrayIn=IIC_RxCycle(0);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegWriteN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayOut)
{
byte i,err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(FirstReg);
for (i=0;i<Nbytes;i++) {err|=IIC_TxCycle(*ArrayOut); ArrayOut++;}
IIC_Stop();
return err;
}
8. Interrupt Bodies
9. NO SCI support for this module
ENDMODULE


MODULE:A/D
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
for(;;)
  {
  adcin=ADC_Read(Pot);                       // read ADC for POT
  MCU_Delay(adcin*10);                       // delay variable with pot position
  LEDR^=1;                                   // flash RED LED in reference to POT position
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:ACMP
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
ACMPSC=0xD3;                                 // ACMP enable, compare to bandgap voltage (1.2V), enable interrupt, rising & falling edges
for (;;)                                     // forever loop
  {
  _asm stop;                                 // stop and wait for interrupt
  ACMPSC_ACF=1;                              // clear interrupt
  if (ACMPSC_ACO) LEDR=1;                    // determine who caused interrupt
  MCU_Delay(1000); LEDR=0;                   // pulse LED for 100ms
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:RTI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
SRTISC=0x17;                                 // RTI from internal 1kHz clock, interrupts enabled, period=1024ms
for (;;)                                     // forever loop
  {
  _asm stop;                                 // stop and wait for interrupt
  SRTISC_RTIACK=1;                           // clear interrupt 
  Buzzer(1); LEDR=1; MCU_Delay(500);         // Buzzer and Red LED ON for 50ms
  Buzzer(0); LEDR=0;                         // then back off
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:STOPMODE
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
LEDR=1;                                      // RED LED until SW4 is pressed
_asm(nop);                                   // small delay
while (nSW4);                               // wait for SW4 to be pressed
PTBD =0x00;
PTCD =0x00;
PTAD =0x00;                                  // set all GPIO to a low or high state
PTADD=0xFF;                                  // Set all GPIO to an output state
PTBDD=0xFF;
PTCDD=0xFF;
MCU_Delay(30000);                            // delay 3 seconds before entering STOP2 mode
LEDR=0;                                      // RED LED off
_asm stop;                                   // STOP instruction, run directly in assembly
for (;;);                                    // Important!: BDM cable must be unplugged from demo board
                                             // or target board. Measured current=0.25uA
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:KBI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
KBIES=0x00;                                  // detect on falling edges
KBIPE=0x08;                                  // enable KBIP3
KBISC_KBACK=1;                               // clear interrupt
KBISC=0x02;                                  // KBI interrupts enabled, edges only
for (;;)                                     // forever loop
  {
  _asm stop;                                 // stop and wait for interrupt
  KBISC_KBACK=1;                             // clear interrupt
  if (!nSW4) 
  {LEDR=1; MCU_Delay(10000); LEDR=0;}        // pulse LED for 1s
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:TPM_PWM
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte mode;                                   // mode tracks the 2 PWM options
5. Function Headers
6. Main
MCU_Init();
mode=0;                                      // start in mode 0 (LEDs off, no buzzer)
for (;;)
  {
  if ((!nSW4)&&(mode!=2))                    // only enter mode 2 if SW4 pressed and not already in mode 2
    {
    TPMSC=0x08;                              // busclk/1=125ns per count; buzzer on       
    TPMC0SC=0x28;                            // edge-aligned PWM
    TPMMOD=18182-1;                          // 440Hz
    TPMC0V=18182>>1;                         // half of the above to produce 50% duty cycle PWM
    LEDR=1; mode=2;                          // turn RED LED on, and remember that we are in mode 2
    }
  else if ((nSW4)&&(mode!=0))                // only enter mode 0 if neither SW4 pressed, and not already in mode 0
    {
    TPMSC=0x00;                              // busclk/1=125ns per count; buzzer off
    LEDR=0; mode=0;                          // turn LEDs off, and remember that we are in mode 0
    } 
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:BOARD_TEST
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
for (;;)                                     // Factory Board Test
  {
  LEDR^=1;                                   // toggle Red LED
  if (!nSW4)                                 // in SW4 pressed,
    {
    Buzzer(1);                               // sound buzzer
    while (!nSW4);
    Buzzer(0);
    }
  adcin=ADC_Read(Pot);
  MCU_Delay(adcin*10);                       // delay variable with pot position
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


PRMSTART
/* This is a linker parameter file for the mc9rs08kb2 */

NAMES END /* CodeWarrior will pass all the needed files to the linker by command line. But here you may add your own files too. */

SEGMENTS /* Here all RAM/ROM areas of the device are listed. Used in PLACEMENT below. */
    TINY_RAM                 =  READ_WRITE   0x0005 TO 0x000D;
    INT_RAM                  =  READ_WRITE   0x0050 TO 0x0054;
    RAM                      =  READ_WRITE   0x0055 TO 0x00BF;
    RESERVED_RAM             =  NO_INIT      0x0000 TO 0x0004;
    ROM                      =  READ_ONLY    0x3800 TO 0x3FF6;
END

PLACEMENT /* Here all predefined and user segments are placed into the SEGMENTS defined above. */
    RESERVED                 INTO RESERVED_RAM;
    TINY_RAM_VARS            INTO TINY_RAM;
    DIRECT_RAM_VARS          INTO RAM, TINY_RAM;
    DEFAULT_RAM              INTO RAM, TINY_RAM;
    DEFAULT_ROM              INTO ROM;
END

STACKSIZE 0x00 /* no stack for RS08 */

VECTOR 0 _Startup /* Reset vector: this is the default entry point for an application. */
PRMEND
