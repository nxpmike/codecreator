Revision=8
MenuName=M0+_KL03
MenuComment=ARM Cortex 32-bit/M0+ Core/32KB Flash/2KB RAM/
MCUID=
CURRENTDEVICE=M0+_KL03
Hardware=FRDM-KL03Z


MODULE:INIT
ModuleComment=This is the absolute minimum initialization needed./It shows you how to blink an LED/using a Delay loop with the ARM Core SysTick Timer on the FRDM-KL03Z board/TESTED in IAR v7.10.3 on 27MAY2014./
1. Includes
#include "derivative.h"                     // KE03 M0+ register definitions
2. Defines
#define Overflow1    (SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk)  // used by MCU_Delay()
#define ADC_Chan     0x02                   // Connect POT wiper lead to FRDM-KL03Z board, J4-1
#define nSW2         (PTB->PDIR&(1<<0))     // Switch 1, negative logic (0=pressed, 1=not pressed)
#define nSW3         (PTB->PDIR&(1<<5))     // Switch 2, negative logic (0=pressed, 1=not pressed)
3. Global Constants
4. Global Variables
long adcin;                                  // adcin will store ADC results
5. Function Headers
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(long delay);                  // delay in multiples of 1ms
//---------------------------------------------------------------------
void RGB(int Red,int Green,int Blue);        // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
long ADC_Read(int x);                        // Analog-to-Digital Converter byte read: enter channel to read

6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off
for(;;)                                      // forever loop 
  { 
   RGB(1,0,0);                               // Turn on RED LED 
   MCU_Delay(100);                           // Delay 100 msec
   RGB(0,0,0);                               // Turn on GREEN LED 
   MCU_Delay(100);                           // Delay 100 msec 
  } 
7. Function Bodies
void MCU_Init(void) 
{
// Disable Interrupts                       
__asm("CPSID i");                            // Disable interrupts

// System Registers
SIM->COPC=0x00;                              // Disable COP watchdog
SIM->SCGC5|=SIM_SCGC5_PORTA_MASK;            // Enable clock gate for PORTA registers
SIM->SCGC5|=SIM_SCGC5_PORTB_MASK;            // Enable clock gate for PORTB registers
SIM->SCGC6|=SIM_SCGC6_ADC0_MASK;             // Enable clock gate for ADC registers
SIM->SCGC5|=SIM_SCGC5_LPUART0_MASK;          // Enable clock gate for LPUART0 registers
SIM->SCGC6|=SIM_SCGC6_TPM0_MASK;             // Enable clock gate for TPM1 registers
//SIM->SOPT2|=SIM_SOPT2_CLKOUTSEL(7);          // Enable CLKOUT pin for HIRC 48MHz
SIM->SOPT2|=SIM_SOPT2_TPMSRC(1);             // Enable HIRC 48MHz CLK for TPM1 clock
SIM->SOPT2|=SIM_SOPT2_UART0SRC(1);           // Enable HIRC 48MHz CLK as the LPUART0 input clock
SIM->SCGC4|=SIM_SCGC4_CMP_MASK;              // Enable CMP0 module  

// Errata 8068 fix
SIM->SCGC6 |= SIM_SCGC6_RTC_MASK;            // enable clock to RTC   
RTC->TSR = 0x00;                             // dummy write to RTC TSR per errata 8068             
SIM->SCGC6 &= ~SIM_SCGC6_RTC_MASK;           // disable clock to RTC

// System Tick Init
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=48000;                         // Busclk/1=LIRC 48MHz, Period=1ms/(1/48000000Hz)=48000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock) 

// ADC Configuration
ADC0->CFG1=(ADC_CFG1_ADIV(2)                 // Set ADC input clock /4
           |ADC_CFG1_MODE(1)                 // Set ADC conversion mode to 12-bit
           |ADC_CFG1_ADICLK(0));             // Set ADC input to BUS CLK

// System clock initialization
MCG->MC|=MCG_MC_HIRCEN_MASK;                 // Enable 48MHz HIRC
MCG->C1=MCG_C1_CLKS(0);                      // Enable Main Clock Source of the 48MHz HIRC

// Timer0 Init
TPM0->SC=0x00;                               // busclk/1=20.80ns per count; buzzer on=0x08, off=0x00
TPM0->CONTROLS[1].CnSC=0x28;                 // edge-aligned PWM, Output on FRDM-KL03Z Board on J2-3
TPM0->MOD=48000;                             // 1KHz by default
TPM0->CONTROLS[1].CnV=48000>>1;              // half of the above to produce 50% duty cycle PWM

// GPIO Init
PORTB->PCR[10] = PORT_PCR_MUX(1);            // Set pin MUX for GPIO on PORTB pin 10
PORTB->PCR[11] = PORT_PCR_MUX(1);            // Set pin MUX for GPIO on PORTB pin 11
PORTB->PCR[13] = PORT_PCR_MUX(1);            // Set pin MUX for GPIO on PORTB pin 13
PORTA->PCR[9] = PORT_PCR_MUX(0);             // Set pin MUX for ADC0_SE2 on PORTA pin 9
PORTA->PCR[5] = PORT_PCR_MUX(2);             // Set pin MUX for TPM0CH1 on PORTA pin 5
PORTB->PCR[1] = PORT_PCR_MUX(2);             // Set pin MUX for LPUART0 TX on PORTB pin 1
PORTB->PCR[2] = PORT_PCR_MUX(2);             // Set pin MUX for LPUART0 RX on PORTB pin 2
PORTA->PCR[12] = PORT_PCR_MUX(0);            // Set pin MUX for CMP0_IN0 on PORTA pin 12
PORTB->PCR[0] = PORT_PCR_MUX(1)|0x03;        // SW2 is now a GPIO, with pullup enabled (negative logic)
PORTB->PCR[5] = PORT_PCR_MUX(1)|0x03;        // SW3 is now a GPIO, with pullup enabled (negative logic)
PTB->PDDR|=(1<<10);                          // Set pin as output for Red LED
PTB->PDDR|=(1<<11);                          // Set pin as output for Green LED
PTB->PDDR|=(1<<13);                          // Set pin as output for Blue LED
	 
}
//---------------------------------------------------------------------
void MCU_Delay (long delay)                  // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{long delw; for (delw=0;delw<delay;delw++) {while (!Overflow1);}}
//---------------------------------------------------------------------
void RGB(int Red,int Green,int Blue)         // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) PTB->PCOR|=(1<<10);  else PTB->PSOR|=(1<<10);
if (Green ==1) PTB->PCOR|=(1<<11);  else PTB->PSOR|=(1<<11);
if (Blue  ==1) PTB->PCOR|=(1<<13);  else PTB->PSOR|=(1<<13);
}
//---------------------------------------------------------------------
long ADC_Read(int x)                         // Analog-to-Digital Converter byte read: enter channel to read
{ADC0->SC1[0]=ADC_SC1_ADCH(x); while ((ADC0->SC1[0]&ADC_SC1_COCO_MASK)==0); 
return ADC0->R[0];
}
//---------------------------------------------------------------------
8. Interrupt Bodies
ENDMODULE


MODULE:LPUART0
ModuleComment=Simple use of LPUART0 on the FRDM-KL03Z board. Use Device Mgr to find the COMx port/to communicate with a terminal program with a BR=115200,8-bit,No Par,1 SB,No Flow. Press a/key on the Keyboard and the RED LED lights and the terminal outputs the ASCII character on the/terminal. Tested on a FRDM-KL03Z board using in IAR v7.20.1 on 30AUG2014./
1. Includes
2. Defines
3. Global Constants
4. Global Variables
int cin;
5. Function Headers
//---------------------------------------------------------------------
void UART_Init(void);                         // initializes the serial port
void UART_CharOut(int data);                  // sends out a character
void UART_NibbOut(int data);                  // sends out a nibble (hex value)
void UART_ByteOut(int data);                  // sends out a byte (hex value)
void UART_TextOut(char *data);                // sends out a string
int  UART_CharIn(void);                       // reads a character
int  UART_ByteIn(void);                       // reads two characters and interprets them as a byte (hex)
void UART_CRLF(void);                         // send a carriage return + line feed
6. Main
MCU_Init();                                   // MCU Initialization; has to be done prior to anything else
UART_Init();                                  // also initialize the UART 
RGB(0,0,0);
for(;;)                                       // forever loop 
  {
  cin=UART_CharIn();                          // read a character from the UART (probably from your PC)
  RGB(1,0,0);                                 // Flash RED LED when key is pressed
  MCU_Delay(5); 
  RGB(0,0,0);                                
  if (cin=='!')                               // if the character is a '!',
    UART_TextOut("KL03 Sample LPUART0 Code"); // then send out some text,
  else                                        // otherwise,
    {
    UART_CharOut(cin);                        // show the character,
    UART_CharOut('=');                        // and also,
    UART_ByteOut(cin);                        // the ASCII value in hex
    }
  UART_CRLF();                                // go to the next line                   
  } 
7. Function Bodies
//---------------------------------------------------------------------
// UART FUNCTIONS //////////////////////////////////////////////////////
//---------------------------------------------------------------------
void UART_Init(void)          {LPUART0->CTRL=0;  LPUART0->BAUD=LPUART_BAUD_SBR(26); LPUART0->CTRL=(LPUART_CTRL_RE_MASK|LPUART_CTRL_TE_MASK);}   // 48MHz/16/26=115,200 baud (actual 115,384) $$$$ IMPORTANT NOTE:  On the FRDM-KL03Z board, C35 needs to be removed if UART baud rate is greater than 38,400.
//---------------------------------------------------------------------
void UART_CharOut(int data)   {while ((LPUART0->STAT&LPUART_STAT_TDRE_MASK)==0); LPUART0->DATA=data;}
//---------------------------------------------------------------------
void UART_NibbOut(int data)   {if (data>0x09) UART_CharOut(data+0x37); else UART_CharOut(data+0x30);}
//---------------------------------------------------------------------
void UART_ByteOut(int data)   {UART_NibbOut(data>>4); UART_NibbOut(data&0x0F);}
//---------------------------------------------------------------------
void UART_TextOut(char *data) {while (*data!=0x00) {UART_CharOut(*data); data++;}}
//---------------------------------------------------------------------
int UART_CharIn(void)         {while ((LPUART0->STAT&LPUART_STAT_RDRF_MASK)==0); return ((LPUART0->DATA)&(0x000000FF));}  
//---------------------------------------------------------------------
int UART_ByteIn(void)         {int h,l; h=UART_CharIn()-48; if (h>9) h-=7; l=UART_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
//---------------------------------------------------------------------
void UART_CRLF(void)          {UART_CharOut(0x0D); UART_CharOut(0x0A);}
8. Interrupt Bodies
ENDMODULE



MODULE:A/D
ModuleComment=The RGB LED on the board will blink each color in reference/to how much a POT, connected to J4-1, is/turned CCW or CW. Tested on a FRDM-KL03Z board/using in IAR v7.10.3 on 27MAY2014./
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
for(;;)
  { 
   adcin=ADC_Read(ADC_Chan);                 // Read ADC0 Channel 2 from connector J4-1  
   MCU_Delay(adcin);                         // Delay time is A/D value  
   RGB(1,0,0);                               // Turn on Red LED
   adcin=ADC_Read(ADC_Chan);                 // Read ADC0 Channel 2 from connector J4-1   
   MCU_Delay(adcin);                         // Delay time is A/D value
   RGB(0,1,0);                               // Turn on Green LED
   adcin=ADC_Read(ADC_Chan);                 // Read ADC0 Channel 2 from connector J4-1
   MCU_Delay(adcin);                         // Delay time is A/D value
   RGB(0,0,1);                               // Turn on Blue LED
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:ANALOG_COMP
ModuleComment=The Analog Comparator uses a POT connected to Pin J4-3 on the FRDM-KL03Z Board./Turn the POT fully CCW and the GREEN LED will turn ON indicating the CMP0 output is low./Turn the POT fully CW and the RED LED will turn ON indicating the CMP0 output is high./Tested on a FRDM-KL03Z board using in IAR v7.10.3 on 03JUNE2014./
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init(); 
PORTB->PCR[2] = PORT_PCR_MUX(0);              // Set pin MUX for CPM0_IN3
               
RGB(0,0,0);                                   // Start with all LEDs off
CMP0->CR1=0x00;                               // Comparator Disabled
CMP0->CR0=0x00;                               // Filter disabled, Hyst ctrl is Level 0 = 5mV
CMP0->FPR=0x00;                               // Filter disabled
CMP0->MUXCR=0x07;                             // PSEL is IN0, MSEL is 6-Bbit DAC REF
CMP0->DACCR=0xE0;                             // DAC Enable, DAC Vin2in is VDD, DAC0 is 32 DACO=(Vin/64)*(VOSEL[5:0]+1)=1.65V
CMP0->CR1=0x01;                               // Continuous Mode, SE=0, WE=0, CMP ouput pin disabled, Comapator Enabled
for(;;)
  { 
  if ((CMP0->SCR&CMP_SCR_COUT_MASK)==1)       // Check comparator output COUT
  {RGB(1,0,0);}                               // Turn on RED LED if COUT is high
   
  if ((CMP0->SCR&CMP_SCR_COUT_MASK)==0)       // Check comparator output COUT
  {RGB(0,1,0);}                               // Turn on GREEN LED if COUT is low
  }   
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:TPM_PWM
ModuleComment=Two PWMs are output on TPM0_CH1, PTA5, J2-3 on the FRDM-KL03Z Board./Press SW2 and a 1KHz square wave is indicated by the RED LED,/press SW3 and a 10KHz square wave is indicated by the GREEN LED, then followed by a 1 sec/pause. Tested on a FRDM-KL03Z Board using in IAR v7.10.3 on 02JUNE2014./
1. Includes
2. Defines
3. Global Constants
4. Global Variables
int mode;                                    // mode tracks the 2 PWM options
5. Function Headers
6. Main
MCU_Init();
mode=0;                                      // start in mode 1 (LEDs off)
RGB(0,0,0);                                  // LEDs OFF
for (;;)
   {
    if ((!nSW2)&&(mode!=1))                  // only enter mode 1 if SW2 pressed and not already in mode 1
    {
    TPM0->SC=0x08;                           // busclk/1=20.80ns per count; buzzer on=0x08, off=0x00
    TPM0->CONTROLS[1].CnSC=0x28;             // edge-aligned PWM, Output on FRDM-KL03Z Board on J2-3
    TPM0->MOD=48000;                         // 1KHz by default
    TPM0->CONTROLS[1].CnV=48000>>1;          // half of the above to produce 50% duty cycle PWM
    RGB(1,0,0); mode=1;                      // turn Red LED on, and remember that we are in mode 2
    }
  else if ((!nSW3)&&(mode!=2))               // only enter mode 2 if SW3 pressed and not already in mode 2
    {
    TPM0->SC=0x08;                           // busclk/1=20.80ns per count; buzzer on=0x08, off=0x00
    TPM0->CONTROLS[1].CnSC=0x28;             // edge-aligned PWM, Output on FRDM-KL03Z Board on J2-3
    TPM0->MOD=4800;                          // 10KHz by default
    TPM0->CONTROLS[1].CnV=4800>>1;           // half of the above to produce 50% duty cycle PWM
    RGB(0,1,0); mode=2;                      // turn Green LED on, and remember that we are in mode 1
    }
  else if ((nSW2)&&(nSW3)&&(mode!=0))        // only enter mode 0 if neither SW2 nor SW3 pressed, and not already in mode 0
    {
    TPM0->SC=0x00;                           // busclk/1=20.80ns per count; buzzer on=0x08, off=0x00
    RGB(0,0,0); mode=0;                      // turn LEDs off, and remember that we are in mode 0
    } 
   } 
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:BOARD_TEST
ModuleComment=The RGB LED on the board will blink RED, GREEN, BLUE in/sequence at 1 sec rate. Tested on a FRDM-KE02Z board/using in CW v10.5 and IAR v6.7.03 on 17FEB2014./
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off

for(;;)                                      // forever loop 
  {
   RGB(1,0,0);                               // Turn of RED LED 
   MCU_Delay(1000);                          // Delay 1 sec
   RGB(0,1,0);                               // Turn of GREEN LED 
   MCU_Delay(1000);                          // Delay 1 sec 
   RGB(0,0,1);                               // Turn of BLUE LED 
   MCU_Delay(1000);                          // Delay 1 sec 
  } 

7. Function Bodies
8. Interrupt Bodies
ENDMODULE
