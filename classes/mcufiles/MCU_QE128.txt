Revision=4
MenuName=S08_QE128
MenuComment=8-bit/S08 Core/128KB Flash/4KB RAM/
IncludeName=<MC9S08QE128.h>
MCUID=0x1040
CURRENTDEVICE=9S08QE128


MODULE:INIT
1. Includes
#include "derivative.h"                      // gives acces to all MCU registers
2. Defines
#define Overflow1    TPM1SC_TOF              // used by MCU_Delay()
#define LEDR         PTBD_PTBD5              // Red   LED, positive logic (1=on, 0=off); to do PWM dimming, use TPM1CH1 instead
#define LEDG         PTBD_PTBD4              // Green LED, positive logic (1=on, 0=off); to do PWM dimming, use TPM2CH1 instead
#define LEDB         PTCD_PTCD3              // Blue  LED, positive logic (1=on, 0=off); to do PWM dimming, use TPM3CH3 instead
#define nSW3         PTDD_PTDD6              // Switch 3,  negative logic (0=pressed, 1=not pressed)
#define nSW4         PTDD_PTDD7              // Switch 4,  negative logic (0=pressed, 1=not pressed)
#define Pot          0x01                    // Potentiometer is on A/D channel 0x01
3. Global Constants
4. Global Variables
byte R,G,B,adcin;                            // R,G,B are LED flags; adcin will store ADC results
5. Function Headers
void MCU_Init(void);                         // initializes MCU for the Universal S08 Tower Board
void MCU_Delay(word delay);                  // delay in multiples of 100us
//---------------------------------------------------------------------
void RGB(byte Red,byte Green,byte Blue);     // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
void Buzzer(byte OnOff);                     // Buzzer Control:  1=on, 0=off
byte ADC_Read(byte chan);                    // Analog-to-Digital Converter byte read: enter channel to read
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
R=G=B=0;                                     // start with LEDs off
for (;;)                                     // forever loop
  {
  R^=1;                                      // toggle Red LED on each cycle
  if (!nSW3) {B=1; Buzzer(1);} else B=0;     // pressing SW3 turns Buzzer  on, and adds Blue  LED while pressed
  if (!nSW4) {G=1; Buzzer(0);} else G=0;     // pressing SW4 turns Buzzer off, and adds Green LED while pressed 
  adcin=ADC_Read(Pot);                       // take an 8-bit A/D reading of the Pot
  TPM2MOD=adcin<<8;                          // shift Pot value left 8 bits, to create 16-bit timer period value (modulo) 
  TPM2C2V=adcin<<7;                          // shift Pot value left 7-bits, to create half of the above: 50% duty cycle PWM
  RGB(R,G,B);                                // turn on corresponding LEDs
  MCU_Delay(5000);                           // run a 5000 x 100us = 0.5s delay
  }
7. Function Bodies
void MCU_Init(void) 
{
// Crucial            
_asm SEI;                                    // disable interrupts
SOPT1=0x23;                                  // disable COP, enable Stop mode; Reset & Bkgd = BDM
SPMSC1=0x00;                                 // disable LVDSE
SPMSC2=0x03;                                 // enable Stop2 Mode
SPMSC3=0x00;                                 // disable LVW
// System Clock Init
ICSTRM=NVICSTRM;                             // initialize ICSTRM register from NV_ICSTRM
ICSSC =NVFTRIM;                              // initialize ICSSC  register from NV_FTRIM
ICSC1=0x04;
ICSC2=0x00;                                  // go to full bus speed (/1) instead of /2
ICSSC|=0x80;                                 // Trim*1536=32768Hz*1536=50.332MHz (25.166MHz bus)
// ADC Init
ADCCFG=0x40;                                 // 8 bits & 6.3MHz clock (/4)
// Timer1 Init
TPM1SC=0x08;                                 // busclk/1=39.736ns per count
TPM1MOD=2517-1;                              // 2517 * 39.736ns = 100us period
// Timer2 Init (Buzzer)
TPM2SC=0x00;                                 // busclk/1=39.736ns per count; buzzer on=0x08, off=0x00
TPM2C2SC=0x28;                               // edge-aligned PWM
TPM2MOD=57195-1;                             // 440Hz by default
TPM2C2V=57194>>1;                            // half of the above to produce 50% duty cycle PWM
// GPIO Init
PTBD=0x00;                                   // LEDR (PTB5) and LEDG (PTB4) off by default                                   
PTCD=0x00;                                   // LEDB (PTC3) off by default
PTBDD=0x30;                                  // LEDR (PTB5) and LEDG (PTB4) are outputs
PTCDD=0x08;                                  // LEDB (PTC3) is an output
PTDPE=0xC0;                                  // enable pull-ups for nSW4 (PTD7) and nSW3 (PTD6)
}
//---------------------------------------------------------------------
void MCU_Delay(word delay)                   // Delay in multiples of 100us (e.g. use 10000 for 1 second)
{word delw; for (delw=0;delw<delay;delw++) {while (!Overflow1); Overflow1=0;}}
//---------------------------------------------------------------------
void RGB(byte Red,byte Green,byte Blue)      // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red  ==1) LEDR=1; else LEDR=0;
if (Green==1) LEDG=1; else LEDG=0;
if (Blue ==1) LEDB=1; else LEDB=0;
}
//---------------------------------------------------------------------
void Buzzer(byte OnOff)                      // Buzzer Control:  1=on, 0=off
{if (OnOff==1) TPM2SC=0x08; else TPM2SC=0x00;}
//---------------------------------------------------------------------
byte ADC_Read(byte chan)                     // Analog-to-Digital Converter byte read: enter channel to read
{ADCSC1=chan; while (!ADCSC1_COCO); return ADCRL;}
8. Interrupt Bodies
ENDMODULE


MODULE:SCI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte cin;
5. Function Headers
//---------------------------------------------------------------------
void SCI_Init(void);                         // initializes the serial port
void SCI_CharOut(byte data);                 // sends out a character
void SCI_NibbOut(byte data);                 // sends out a nibble (hex value)
void SCI_ByteOut(byte data);                 // sends out a byte (hex value)
void SCI_TextOut(byte *data);                // sends out a string
byte SCI_CharIn(void);                       // reads a character
byte SCI_ByteIn(void);                       // reads two characters and interprets them as a byte (hex)
void SCI_CRLF(void);                         // send a carriage return + line feed
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
SCI_Init();                                  // also initialize the SCI  
for(;;)                                      // forever loop 
  {
  cin=SCI_CharIn();                          // read a character from the SCI (probably from your PC)
  if (cin=='!')                              // if the character is a '!',
    SCI_TextOut("SCI Test.");                // then send out some text,
  else                                       // otherwise,
    {
    SCI_CharOut(cin);                        // show the character,
    SCI_CharOut('=');                        // and also,
    SCI_ByteOut(cin);                        // the ASCII value in hex
    }
  SCI_CRLF();                                // go to the next line                     
  } 
7. Function Bodies


// SCI FUNCTIONS //////////////////////////////////////////////////////
//---------------------------------------------------------------------
void SCI_Init(void)          {SCI1BD=41; SCI1C1=0x00; SCI1C2=0x0C; SCI1S2=0x00; SCI1C3=0x00;}   // 25.166MHz/16/41=38,400 baud (actually 38,363)
//---------------------------------------------------------------------
void SCI_CharOut(byte data)  {while (!SCI1S1_TDRE); SCI1D=data;}
//---------------------------------------------------------------------
void SCI_NibbOut(byte data)  {if (data>0x09) SCI_CharOut(data+0x37); else SCI_CharOut(data+0x30);}
//---------------------------------------------------------------------
void SCI_ByteOut(byte data)  {SCI_NibbOut(data>>4); SCI_NibbOut(data&0x0F);}
//---------------------------------------------------------------------
void SCI_TextOut(byte *data) {while (*data!=0x00) {SCI_CharOut(*data); data++;}}
//---------------------------------------------------------------------
byte SCI_CharIn(void)        {while (!SCI1S1_RDRF); return SCI1D;}
//---------------------------------------------------------------------
byte SCI_ByteIn(void)        {byte h,l; h=SCI_CharIn()-48; if (h>9) h-=7; l=SCI_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
//---------------------------------------------------------------------
void SCI_CRLF(void)          {SCI_CharOut(0x0D); SCI_CharOut(0x0A);}
8. Interrupt Bodies
ENDMODULE


MODULE:SPI uses SCI
1. Includes
2. Defines
#define nSS          PTDD_PTDD3              // used by SPI_SSn0() and SPI_SSn1()
3. Global Constants
4. Global Variables
byte bufout[20],bufin[20],val,h,l,res,p,i;
5. Function Headers
//---------------------------------------------------------------------
void SPI_Init(void);
void SPI_nSS0(void);
void SPI_nSS1(void);
byte SPI_CharShift(byte mosi);
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
SCI_Init();                                  // use SCI to enter data and display results
SPI_Init();                                  // initialize SPI
for(;;)
  {
  cin=SCI_CharIn();                          // wait for 's' before reading data
  if (cin=='s')
    {
    SCI_TextOut("Bytes to send   = [");      // put data to be sent between '[' and ']'
	  p=0;                                     // index to output array
	  do
	    {
	    val=SCI_CharIn();                      // read a character
	    if (val!=0x0D)                         // if not "Enter", interpret as a byte, and store in buffer
	      {
		    h=val-48; if (h>9) h-=7; l=SCI_CharIn()-48; if (l>9) l-=7; res=(h&15)*16+(l&15);
		    bufout[p++]=res; SCI_ByteOut(res); SCI_CharOut(' ');
		    }
	    } while ((val!=0x0D)&&(p<20));         // store up to 20 bytes
	  SCI_CharOut(']'); SCI_CRLF();            // close bracket, send carriage return and line feed
	  SPI_nSS0();                                                   // Assert Chip Select!
	  for (i=0;i<p;i++) bufin[i]=SPI_CharShift(bufout[i]);          // Send Bytes, and store results in buffer
	  SPI_nSS1();                                                   // DeAssert Chip Select
	  SCI_TextOut("Bytes read back = [");
	  for (i=0;i<p;i++) {SCI_ByteOut(bufin[i]); SCI_CharOut(' ');}  // Display Results
    SCI_CharOut(']'); SCI_CRLF();
    }
  else                                       // explain usage 
    {
    SCI_TextOut("Press 's' to Send: enter up to 20 bytes in hex, and press enter to finish block.");
	  SCI_CRLF();
    }
  }
7. Function Bodies


// SPI FUNCTIONS (MASTER) /////////////////////////////////////////////
//---------------------------------------------------------------------
void SPI_Init(void)
{
SPI2C1=0x50;                                 // no interrupts, master, CPOL=CPHA=0, SS*=GPIO, msb first
SPI2C2=0x00;                                 // standard
SPI2BR=0x02;                                 // 24MHz/8= 3MHz
PTDD_PTDD3=1;                                // nSS=1
PTDDD_PTDDD3=1;                              // make PTD3 nSS an output
}
//---------------------------------------------------------------------
void SPI_nSS0(void)           {nSS=0;}
//---------------------------------------------------------------------
void SPI_nSS1(void)           {nSS=1;}
//---------------------------------------------------------------------
byte SPI_CharShift(byte mosi) {while (!SPI2S_SPTEF); SPI2D=mosi; while (!SPI2S_SPRF); return SPI2D;}
8. Interrupt Bodies
ENDMODULE


MODULE:IIC uses SCI
1. Includes
2. Defines
#define IIC_SDA      PTHD_PTHD7              // change to match your GPIO
#define IIC_SCL      PTHD_PTHD6              // change to match your GPIO
#define IIC_DD_SDA   PTHDD_PTHDD7            // change to match your GPIO
#define IIC_DD_SCL   PTHDD_PTHDD6            // change to match your GPIO
3. Global Constants
4. Global Variables
byte slave,reg,val,cmd;
word found;
5. Function Headers
//---------------------------------------------------------------------
byte IIC_Init(void);
word IIC_Find(byte LastAddress);
byte IIC_SlaveRegRead(byte SlaveAddress,byte Reg,byte *Result);
byte IIC_SlaveRegWrite(byte SlaveAddress,byte Reg,byte Val);
byte IIC_SlaveRegReadN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayIn);
byte IIC_SlaveRegWriteN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayOut);
6. Main
MCU_Init();
SCI_Init();
(void)IIC_Init();
slave=0x01;
for(;;)
  {
  cmd=SCI_CharIn();
  if (cmd=='f')
    {
    found=IIC_Find(0x7F);
    SCI_TextOut("Found "); SCI_ByteOut(found/256); 
    SCI_TextOut(" slaves. Last address = 0x"); SCI_ByteOut(found%256); SCI_CRLF();    
    }
  else if (cmd=='r')
    {
    SCI_TextOut("Read from slave 0x"); SCI_ByteOut(slave); SCI_TextOut(" - register 0x");
    reg=SCI_ByteIn(); SCI_ByteOut(reg);
    (void)IIC_SlaveRegRead(slave,reg,&val);
    SCI_TextOut("--->0x"); SCI_ByteOut(val); SCI_CRLF();
    }
  else if (cmd=='w')
    {
    SCI_TextOut("Write to  slave 0x"); SCI_ByteOut(slave); SCI_TextOut(" - register 0x");
    reg=SCI_ByteIn(); SCI_ByteOut(reg); 
    SCI_TextOut("<---0x");
    val=SCI_ByteIn(); SCI_ByteOut(val);
    (void)IIC_SlaveRegWrite(slave,reg,val); SCI_CRLF();
    }
  else if (cmd=='s')
    {
    SCI_TextOut("Set slave address to: 0x");
    slave=SCI_ByteIn(); SCI_ByteOut(slave); SCI_CRLF();
    }	
  else 
	  {
	  SCI_TextOut("r=Read, w=Write, s=Slave address, f=Find slaves."); SCI_CRLF();
	  }
  }
7. Function Bodies


// IIC FUNCTIONS (MASTER) /////////////////////////////////////////////
//---------------------------------------------------------------------
//.... Private Functions, hidden from main() ..........................
void Slow()      {byte burn; for (burn=0;burn<20;burn++);}
//.....................................................................
void SDA0()      {IIC_SDA=0; IIC_DD_SDA=1; Slow();}
//.....................................................................
void SDA1()      {           IIC_DD_SDA=0; Slow();}
//.....................................................................
void SCL0()      {IIC_SCL=0; IIC_DD_SCL=1; Slow();}
//.....................................................................
void SCL1()      {           IIC_DD_SCL=0; Slow();}
//.....................................................................
void Ack()       {SDA0(); SCL1(); SCL0();}
//.....................................................................
void Nak()       {SDA1(); SCL1(); SCL0();}
//.....................................................................
void IIC_Start() {SDA1(); SCL1(); SDA0(); SCL0();}
//.....................................................................
void IIC_Stop()  {SDA0(); SCL1(); SDA1();}
//.....................................................................
byte IIC_TxCycle(byte bout) 
{
byte i,p;
word timeout;
timeout=0;
p=0x80;
for (i=0;i<8;i++) 
  {
  if (p&bout) SDA1(); else SDA0();
  SCL1(); SCL0();
  p>>=1;
  }
SDA1();
SCL1();
Slow();
while ((IIC_SDA)&&(timeout<0x0100)) timeout++;
SCL0();
SDA1();
Slow();
if (timeout>=0x0100) return 1; else return 0;
}
//.....................................................................
byte IIC_RxCycle(byte ack) 
{
byte i,p,r;
p=0x80;
r=0;
SDA1();
Slow();
for (i=0;i<8;i++) 
  {
  SCL1();
  Slow();
  if (IIC_SDA) r+=p;
  SCL0();
  Slow();
  p>>=1;
  }
SDA1();
if (ack) Ack(); else Nak();
return r;
} 
//---- Public Functions, seen by main() -------------------------------
byte IIC_Init(void)
{
IIC_DD_SDA=0; IIC_DD_SCL=0; IIC_SDA=0; IIC_SCL=0; Slow();
if ((IIC_SDA)&&(IIC_SCL)) return 0; else return 1;
}
//---------------------------------------------------------------------
word IIC_Find(byte LastAddress)              // tries addresses from 0x00 through LastAddress (clamped to 0x7F)
{                                            // returns word; hi-byte indicates how many slaves were found; lo-byte shows address of last one found
byte found,try,slaveaddress,dummy;
(void)IIC_Init();
slaveaddress=0xFF; dummy=0; found=0;
for (try=0x00;try<=(LastAddress&0x7F);try++) if (IIC_SlaveRegRead(try,0,&dummy)==0) {slaveaddress=try; found++;}
return found<<8|slaveaddress;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegRead(byte SlaveAddress,byte Reg,byte *Result)
{
byte err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(Reg);
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1|1);
*Result=IIC_RxCycle(0);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegWrite(byte SlaveAddress,byte Reg,byte Val)
{
byte err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(Reg);
err|=IIC_TxCycle(Val);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegReadN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayIn)
{
byte i,err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(FirstReg);
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1|1);
for (i=0;i<Nbytes-1;i++) {*ArrayIn=IIC_RxCycle(1); ArrayIn++;}
*ArrayIn=IIC_RxCycle(0);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegWriteN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayOut)
{
byte i,err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(FirstReg);
for (i=0;i<Nbytes;i++) {err|=IIC_TxCycle(*ArrayOut); ArrayOut++;}
IIC_Stop();
return err;
}
8. Interrupt Bodies
ENDMODULE


MODULE:A/D
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
for(;;)
  { 
  adcin=ADC_Read(Pot);                       // turn the pot R25, and the RGB LED turns different colors                                          
  if (adcin&0x80) LEDR=1; else LEDR=0;
  if (adcin&0x40) LEDG=1; else LEDG=0;
  if (adcin&0x20) LEDB=1; else LEDB=0;
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:ANALOG_COMP
1. Includes
2. Defines
#define ACMP_OUTPUT  ACMP1SC_ACO 
3. Global Constants
4. Global Variables
byte flag,comparator;                        // flag indicates an interrupt occurred; comparator is the logic output of the analog comparator
5. Function Headers
6. Main
MCU_Init(); 
SPMSC1=0x01;                                 // enable bandgap reference voltage (1.2V) for ACMP+ input
MCU_Init(); 
ACMP1SC=0xD3;                                // enable comparator, bandgap select, interrupts enabled, compare output falling and rising edges
_asm CLI;                                    // enable MCU interrupts
flag=comparator=0;
for(;;)
  {
  if (flag)                                  // do nothing unless flag=1 (when comparator voltage >}
    {
    Buzzer(1); MCU_Delay(500);               // Buzzer and Blue LED ON for 50ms
    Buzzer(0); flag=0;                       // then back off
    if (comparator==1) RGB(1,0,0);
    else RGB(0,1,0);
    }
  }
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vacmpx void ANALOG_COMPARATOR(void)
{
flag=1;                                     // notify main()
if (ACMP_OUTPUT) comparator=1; 
else comparator=0;
ACMP1SC_ACF=1;                              // Clear ACF flag before exiting
}
ENDMODULE


MODULE:EXTCLOCK
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte internal;
5. Function Headers
6. Main
MCU_Init();                                  // running at 25.2MHz bus
internal=1;                                  // starting with internal clock (initialized in MCU_Init)
for (;;)
  {
  if ((!nSW3)&&(internal==1))                // only switch to external (FBE) if SW3 pressed and running internal oscillator
    {  
    ICSC1=0x80;                              // 4MHz external Ref Clock selected, FLL bypassed
    ICSC2=0x24;                              // Bus speed (/1), Range 1, Ext Osc select 
    ICSSC&=~0x80;                            // 4MHz/2=2MHz (2.0MHz bus)
    internal=0;
    }
  if ((!nSW4)&&(internal==0))                // only switch to internal (FEI) if SW4 pressed and running external oscillator
    {  
    ICSC1=0x44;                              // FLL bypassed (FBI), using internal reference clock
    while (!ICSSC_IREFST);
    ICSC1=0x04;                              // FLL selected (FEI), using internal reference clock
    ICSC2=0x00;
    ICSSC|=0x80;                             // Trim*1536=32768Hz*1536=50.332MHz (25.166MHz bus)
    internal=1;
    }
  Buzzer(1); LEDR=1; MCU_Delay(300);         // Buzzer and Red LED ON
  Buzzer(0); LEDR=0; MCU_Delay(300);         // Buzzer and Red LED OFF
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:STOPMODE
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
IRQSC_IRQPE=1;                               // must have IRQ Pull-up enabled, otherwise MCU will exit STOP2 mode immediately
RGB(0,1,0);                                  // Green LED until SW3 is pressed
while (nSW3);                                // wait for SW3 to be pressed
PTAD =0x00;                                  // set all GPIO to a low or high state
PTBD =0x00;                                  // so they will not draw excessive current
PTCD =0x00;
PTDD =0x00; 
PTED =0x00;
PTADD=0xFF;                                  // Set all GPIO to an output state
PTBDD=0xFF;                                  // so they will not draw excessive current
PTCDD=0xFF;                                  // be careful with bus conflicts!
PTDDD=0xFF; 
PTEDD=0xFF;
RGB(1,0,0);                                  // Red LED during delay
MCU_Delay(30000);                            // delay 3 seconds before entering STOP2 mode
RGB(0,0,0);                                  // all LEDs off
_asm stop;                                   // STOP instruction, run directly in assembly
for (;;);                                    // Important!: BDM cable must be unplugged from demo board
                                             // or target board. Measured current=0.25uA
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:IRQPIN
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
SOPT1=0x22;                                  // enable RESET pin as an IRQ pin input
MCU_Init();                                  // only the first write to the SOPT1 is valid, hence the above line
_asm CLI;                                    // enable CPU interrupts
IRQSC=0x12;                                  // pull-up enable, falling edge sensitive, interrupt request (no polling), falling/rising edge only
RGB(0,1,0);                                  // turn on Green LED while we wait
for (;;);                                    // forever loop; ground IRQ pin (J12-48) to trigger interrupt 
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Virq void EXTERNAL_IRQ(void)
{
RGB(0,0,1);                                  // show interrupt by turning on Blue LED
IRQSC_IRQACK=1;                              // then clear IRQ flag before exiting
}
ENDMODULE


MODULE:RTC
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte flag;                                   // indicates that an RTC interrupt just occurred
5. Function Headers
6. Main
MCU_Init();                                  
flag=0;                                      // start with flag=0
_asm CLI;                                    // enable CPU interrupts
RTCSC=0x1F;                                  // enable RTC to be 1kHz internal / 1000 = 1s
for(;;)
  {
  if (flag)                                 // do nothing unless flag=1
    {
    Buzzer(1); LEDR=1; MCU_Delay(500);      // Buzzer and Red LED ON for 50ms
    Buzzer(0); LEDR=0; flag=0;              // then back off
    }
  }
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vrtc void REAL_TIME_COUNTER(void)
{
flag=1;                                      // change flag for main()
RTCSC_RTIF=1;                                // clear RTC flag before exiting
}
ENDMODULE


MODULE:KBI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  
KBI2SC_KBIE=0;                               // mask interrupts 
KBI2ES=0x00;                                 // enable pullup devices/edge select on KBI pins
KBI2PE=0xC0;                                 // enable KBI2PE6, KBI2PE7 interrupts
KBI2SC=0x02;                                 // interrupt request (no polling), edge only 
_asm CLI;                                    // enable CPU interrupts
KBI2SC_KBACK=1;                              // clear KBI flag
for (;;);                                    // forever loop

7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vkeyboard void Keyboard_Interrupt(void)
{
if (!nSW3) LEDR=1; else LEDR=0;              // have Red   LED reflect status of SW3 when interrupt occurs
if (!nSW4) LEDG=1; else LEDG=0;              // have Green LED reflect status of SW4 when interrupt occurs
KBI2SC_KBACK=1;                              // clear KBI flag before exiting
}
ENDMODULE


MODULE:TPM_PWM
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte mode;                                   // mode tracks the 2 PWM options
5. Function Headers
6. Main
MCU_Init();
mode=0;                                      // start in mode 1 (LEDs off, no buzzer)
for (;;)
  {
  if ((!nSW3)&&(mode!=1))                    // only enter mode 1 if SW3 pressed and not already in mode 1
    {
    TPM2SC=0x08;                             // busclk/1=39.736ns per count; buzzer on  
    TPM2C2SC=0x28;                           // edge-aligned PWM
    TPM2MOD=57195-1;                         // 440Hz
    TPM2C2V=28597;                           // half of the above to produce 50% duty cycle PWM
    RGB(1,0,0); mode=1;                      // turn Red LED on, and remember that we are in mode 2
    }
  else if ((!nSW4)&&(mode!=2))               // only enter mode 2 if SW4 pressed and not already in mode 2
    {
    TPM2SC=0x08;                             // busclk/1=39.736ns per count; buzzer on       
    TPM2C2SC=0x28;                           // edge-aligned PWM
    TPM2MOD=25166-1;                         // 1kHz
    TPM2C2V=2517;                            // 10% of the above to produce 10% duty cycle PWM
    RGB(0,1,0); mode=2;                      // turn Green LED on, and remember that we are in mode 1
    }
  else if ((nSW3)&&(nSW4)&&(mode!=0))        // only enter mode 0 if neither SW3 nor SW4 pressed, and not already in mode 0
    {
    TPM2SC=0x00;                             // busclk/1=39.736ns per count; buzzer off
    RGB(0,0,0); mode=0;                      // turn LEDs off, and remember that we are in mode 0
    } 
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:FLASH_PGM uses SCI
1. Includes
2. Defines
3. Global Constants
// Table in FLASH that contains the assembly code that will run from RAM (starting at location 0x0084)
/* 
a000084 55 80                   ldhx   FLadr
a000086 B6 82                   lda    FLdta
a000088 F7                      sta    0,x
a000089 B6 83                   lda    FLcmd
a00008B C7 1826                 sta    FCMD
a00008E C6 1825                 lda    FSTAT
a000091 AA 80                   ora    #$80
a000093 C7 1825                 sta    FSTAT
a000096 9D                      nop
a000097 9D                      nop
a000098 9D                      nop
a000099 A4 7F                   and    #$7F
a00009B C7 1825                 sta    FSTAT
a00009E C6 1825     wait:       lda    FSTAT
a0000A1 A4 40                   and    #$40
a0000A3 27 F9                   beq    wait            
a0000A5 81                      rts             
*/
const byte PgmFlash[34]={                    // code to program Flash (will xfer to RAM)
0x55,0x80,0xB6,0x82,0xF7,0xB6,0x83,0xC7,0x18,0x26,0xC6,0x18,0x25,0xAA,
0x80,0xC7,0x18,0x25,0x9D,0x9D,0x9D,0xA4,0x7F,0xC7,0x18,0x25,0xC6,0x18,
0x25,0xA4,0x40,0x27,0xF9,0x81};
4. Global Variables
// Variables in RAM that must be filled in before calling the programming routine (make sure other variables do not overwrite these locations!)
word FLadr@0x0080;                           // these variables are mapped to RAM
byte FLdta@0x0082;                           // so Flash programming can use them
byte FLcmd@0x0083;                           // as parameters
byte PgmRAM[34]@0x0084;                      // this table will contain program to run from RAM
byte cmd,adrh,adrl,val;
byte *mem;
word adr16;
5. Function Headers
//---------------------------------------------------------------------
void Flash_BlockErase(word adr);
void Flash_ByteWrite(word adr,byte val);
6. Main
MCU_Init();
SCI_Init();
// Flash Init
FCDIV=0xCF;                                  // 25.17MHz/8/(15+1)=197kHz; must fall close to 200kHz
for(;;)
  {
  cmd=SCI_CharIn();
  if (cmd=='r')
    {
    SCI_TextOut("Read from      address 0x");
    adrh=SCI_ByteIn(); adrl=SCI_ByteIn(); SCI_ByteOut(adrh); SCI_ByteOut(adrl);
    mem=(byte *)(adrh*256+adrl);
    SCI_TextOut("--->0x");
    SCI_ByteOut(*mem);
    SCI_CRLF();
    }
  else if (cmd=='w')
    {
    SCI_TextOut("Write to       address 0x");
    adrh=SCI_ByteIn(); adrl=SCI_ByteIn(); SCI_ByteOut(adrh); SCI_ByteOut(adrl); 
    adr16=adrh*256+adrl;
    SCI_TextOut("<---0x");
    val=SCI_ByteIn(); SCI_ByteOut(val);
    Flash_ByteWrite(adr16,val);
    SCI_CRLF();
    }
  else if (cmd=='e')
    {
    SCI_TextOut("Erase Block at address 0x");
    adrh=SCI_ByteIn(); adrl=SCI_ByteIn(); SCI_ByteOut(adrh); SCI_ByteOut(adrl); 
    adr16=adrh*256+adrl;
    Flash_BlockErase(adr16);
    SCI_CRLF();
    }	
  else 
	  {
	  SCI_TextOut("r=Read, w=Write, e=Erase Block. Reads from bad locations will cause a reset!");
	  SCI_CRLF();
	  }
  }
7. Function Bodies


// Flash FUNCTIONS ////////////////////////////////////////////////////
//---------------------------------------------------------------------
void Flash_BlockErase(word adr)
{
byte i;
for (i=0;i<34;i++) PgmRAM[i]=PgmFlash[i];                 // copy Flash programmer to RAM; need to run code from RAM
FLadr=adr; FLdta=0x00; FLcmd=0x40; _asm jsr $0084;        // erase 512-byte page(for only 3 bytes!!!)
}
//---------------------------------------------------------------------
void Flash_ByteWrite(word adr,byte val)
{
byte i;
for (i=0;i<34;i++) PgmRAM[i]=PgmFlash[i];                 // copy Flash programmer to RAM; need to run code from RAM
FLadr=adr; FLdta=val; FLcmd=0x20; _asm jsr $0084;         // erase 512-byte page(for only 3 bytes!!!)
}
8. Interrupt Bodies
ENDMODULE


MODULE:BOARD_TEST
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
for (;;)                                     // Factory Board Test
  {
  LEDR^=1;                                   // toggle Red LED
  if (!nSW4)                                 // in SW4 pressed,
    {
    Buzzer(1);                               // sound buzzer
    while (!nSW4);
    Buzzer(0);
    }
  adcin=ADC_Read(Pot);
  MCU_Delay(adcin*10);                       // delay variable with pot position
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


PRMSTART
/* This is a linker parameter file for the mc9s08qe128 */

NAMES END /* CodeWarrior will pass all the needed files to the linker by command line. But here you may add your own files too. */

SEGMENTS /* Here all RAM/ROM areas of the device are listed. Used in PLACEMENT below. */
    Z_RAM                    =  READ_WRITE   0x0080 TO 0x00FF;
    RAM                      =  READ_WRITE   0x0100 TO 0x17FF;
    RAM1                     =  READ_WRITE   0x1880 TO 0x207F;
    /* unbanked FLASH ROM */
    ROM                      =  READ_ONLY    0x2080 TO 0x7FFF;
    ROM1                     =  READ_ONLY    0xC000 TO 0xFFAD;
 /* INTVECTS                 =  READ_ONLY    0xFFC0 TO 0xFFFF; Reserved for Interrupt Vectors */
  //OSVECTORS                =  READ_ONLY    0xFFC0 TO 0xFFFF; /* OSEK interrupt vectors (use your vector.o) */
    /* banked FLASH ROM */
    PPAGE_0                  =  READ_ONLY    0x008000 TO 0x00A07F; /* PAGE partially contained in ROM segment */
    PPAGE_2                  =  READ_ONLY    0x028000 TO 0x02BFFF; 
    PPAGE_4                  =  READ_ONLY    0x048000 TO 0x04BFFF; 
    PPAGE_5                  =  READ_ONLY    0x058000 TO 0x05BFFF; 
    PPAGE_6                  =  READ_ONLY    0x068000 TO 0x06BFFF; 
    PPAGE_7                  =  READ_ONLY    0x078000 TO 0x07BFFF; 
 /* PPAGE_1                  =  READ_ONLY    0x018000 TO 0x01BFFF; PAGE already contained in segment at 0x4000-0x7FFF */
 /* PPAGE_3                  =  READ_ONLY    0x038000 TO 0x03BFFF; PAGE already contained in segment at 0xC000-0xFFFF */
END

PLACEMENT /* Here all predefined and user segments are placed into the SEGMENTS defined above. */
    DEFAULT_RAM,                        /* non-zero page variables */
  //.stackstart,                        /* eventually used for OSEK kernel awareness: Main-Stack Start */
    SSTACK,                             /* allocate stack */
  //.stackend,                          /* eventually used for OSEK kernel awareness: Main-Stack End */
                                        INTO  RAM,RAM1;

    _PRESTART,                          /* startup code */
    STARTUP,                            /* startup data structures */
    ROM_VAR,                            /* constant variables */
    STRINGS,                            /* string literals */
    VIRTUAL_TABLE_SEGMENT,              /* C++ virtual table segment */
    NON_BANKED,                         /* runtime routines which must not be banked */
    //.ostext,                          /* OSEK */
    DEFAULT_ROM,
    COPY                                /* copy down information: how to initialize variables */
                                        INTO  ROM; /* ,ROM1: To use "ROM1" as well, pass the option -OnB=b to the compiler */

    PAGED_ROM                           /* routines which can be banked */
                                        INTO  PPAGE_0,PPAGE_2,PPAGE_4,PPAGE_5,PPAGE_6,PPAGE_7,ROM1;

  //OSZeroPage,                         /* this is important as for OSEK performance reasons */
    _DATA_ZEROPAGE,                     /* zero page variables */
    MY_ZEROPAGE                         INTO  Z_RAM;

    //VECTORS_DATA                      INTO OSVECTORS; /* OSEK */
END

ENTRIES /* keep the following unreferenced variables */
    /* OSEK: always allocate the vector table and all dependent objects */
  //_vectab OsBuildNumber _OsOrtiStackStart _OsOrtiStart
END

STACKSIZE 0x200

VECTOR 0 _Startup /* Reset vector: this is the default entry point for an application. */

PRMEND
