Revision=4
MenuName=S08_SH16
MenuComment=8-bit/S08 Core/16KB Flash/512B RAM/
IncludeName=<MC9S08SH16.h>
MCUID=0x01A
CURRENTDEVICE=9S08SH16


MODULE:INIT
1. Includes
#include "derivative.h"                      // gives acces to all MCU registers
2. Defines
#define Overflow1    MTIMSC_TOF              // used by MCU_Delay()
#define LEDR         PTAD_PTAD0              // Red   LED, positive logic (1=on, 0=off)
#define LEDG         PTBD_PTBD3              // Green LED, positive logic (1=on, 0=off)
#define LEDB         PTBD_PTBD4              // Blue  LED, positive logic (1=on, 0=off)
#define nSW4         PTBD_PTBD2              // Switch 4,  negative logic (0=pressed, 1=not pressed)
#define Pot          0x01                    // Potentiometer is on A/D channel 0x01
3. Global Constants
4. Global Variables
byte R,G,B,adcin;                            // R,G,B are LED flags; adcin will store ADC results
5. Function Headers
void MCU_Init(void);                         // initializes MCU for the Universal S08 Tower Board
void MCU_Delay(word delay);                  // delay in multiples of 100us
//---------------------------------------------------------------------
void RGB(byte Red,byte Green,byte Blue);     // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
void Buzzer(byte OnOff);                     // Buzzer Control:  1=on, 0=off
byte ADC_Read(byte chan);                    // Analog-to-Digital Converter byte read: enter channel to read
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
R=G=B=0;                                     // start with LEDs off
for (;;)                                     // forever loop
  {
  }
7. Function Bodies
void MCU_Init(void) 
{
// Crucial            
_asm SEI;                                    // disable interrupts
SOPT1=0x23;                                  // disable COP, enable Stop mode; Reset & Bkgd = BDM
SPMSC1=0x00;                                 // disable LVDSE
SPMSC2=0x01;                                 // enable Stop2 Mode
// System Clock Init
ICSTRM = *(unsigned char*far)0xFFAF;         // Initialize ICSTRM register from NV_ICSTRM
ICSSC  = *(unsigned char*far)0xFFAE;         // Initialize ICSSC  register from NV_FTRIM
ICSC1=0x04;
ICSC2=0x00;                                  // go to full bus speed /1, instead of /2 (default); Trim*1024=31250Hz*1024=32.000MHz (16.000MHz bus)
// ADC Init
ADCCFG=0x20;                                 // 8 bits & 8MHz clock (/2) 
// Modulo Timer Init
MTIMCLK=0x04;                                // busclk/16=1us per count
MTIMMOD=100;                                 // period=100*1us=100us
MTIMSC=0x20;                                 // start clock
// Timer Init (Buzzer)
TPM1SC=0x00;                                 // busclk/1=62.5ns per count; buzzer on=0x08, off=0x00
TPM1C1SC=0x28;                               // edge-aligned PWM
TPM1MOD=36363-1;                             // 440Hz by default
TPM1C1V=36363>>1;                            // half of the above to produce 50% duty cycle PWM
// GPIO Init
PTAD=0x00;                                   // LEDR (PTA0) off by default                                   
PTBD=0x00;                                   // LEDB (PTB4) and LEDG (PTB3) off by default
PTADD=0x01;                                  // LEDR (PTA0) is an output
PTBDD=0x18;                                  // LEDB (PTB4) and LEDG (PTB3) are outputs
PTBPE=0x04;                                  // enable pull-up for nSW4 (PTB2)
}
//---------------------------------------------------------------------
void MCU_Delay(word delay)                   // Delay in multiples of 100us (e.g. use 10000 for 1 second)
{word delw; for (delw=0;delw<delay;delw++) {while (!Overflow1); Overflow1=0;}}
//---------------------------------------------------------------------
void RGB(byte Red,byte Green,byte Blue)      // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red  ==1) LEDR=1; else LEDR=0;
if (Green==1) LEDG=1; else LEDG=0;
if (Blue ==1) LEDB=1; else LEDB=0;
}
//---------------------------------------------------------------------
void Buzzer(byte OnOff)                      // Buzzer Control:  1=on, 0=off
{if (OnOff==1) TPM1SC=0x08; else TPM1SC=0x00;}
//---------------------------------------------------------------------
byte ADC_Read(byte chan)                     // Analog-to-Digital Converter byte read: enter channel to read
{ADCSC1=chan; while (!ADCSC1_COCO); return ADCRL;}
8. Interrupt Bodies
ENDMODULE


MODULE:SCI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte cin;
5. Function Headers
//---------------------------------------------------------------------
void SCI_Init(void);                         // initializes the serial port
void SCI_CharOut(byte data);                 // sends out a character
void SCI_NibbOut(byte data);                 // sends out a nibble (hex value)
void SCI_ByteOut(byte data);                 // sends out a byte (hex value)
void SCI_TextOut(byte *data);                // sends out a string
byte SCI_CharIn(void);                       // reads a character
byte SCI_ByteIn(void);                       // reads two characters and interprets them as a byte (hex)
void SCI_CRLF(void);                         // send a carriage return + line feed
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
SCI_Init();                                  // also initialize the SCI  
for(;;)                                      // forever loop 
  {
  cin=SCI_CharIn();                          // read a character from the SCI (probably from your PC)
  if (cin=='!')                              // if the character is a '!',
    SCI_TextOut("SCI Test.");                // then send out some text,
  else                                       // otherwise,
    {
    SCI_CharOut(cin);                        // show the character,
    SCI_CharOut('=');                        // and also,
    SCI_ByteOut(cin);                        // the ASCII value in hex
    }
  SCI_CRLF();                                // go to the next line                     
  } 
7. Function Bodies


// SCI FUNCTIONS //////////////////////////////////////////////////////
//---------------------------------------------------------------------
void SCI_Init(void)          {SCIBD=26; SCIC1=0x00; SCIC2=0x0C; SCIS2=0x00; SCIC3=0x00;}   // 16MHz/16/26=38,400 baud (actually 38,461)
//---------------------------------------------------------------------
void SCI_CharOut(byte data)  {while (!SCIS1_TDRE); SCID=data;}
//---------------------------------------------------------------------
void SCI_NibbOut(byte data)  {if (data>0x09) SCI_CharOut(data+0x37); else SCI_CharOut(data+0x30);}
//---------------------------------------------------------------------
void SCI_ByteOut(byte data)  {SCI_NibbOut(data>>4); SCI_NibbOut(data&0x0F);}
//---------------------------------------------------------------------
void SCI_TextOut(byte *data) {while (*data!=0x00) {SCI_CharOut(*data); data++;}}
//---------------------------------------------------------------------
byte SCI_CharIn(void)        {while (!SCIS1_RDRF); return SCID;}
//---------------------------------------------------------------------
byte SCI_ByteIn(void)        {byte h,l; h=SCI_CharIn()-48; if (h>9) h-=7; l=SCI_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
//---------------------------------------------------------------------
void SCI_CRLF(void)          {SCI_CharOut(0x0D); SCI_CharOut(0x0A);}
8. Interrupt Bodies
ENDMODULE


MODULE:IIC uses SCI
1. Includes
2. Defines
#define IIC_SDA      PTAD_PTAD2              // change to match your GPIO
#define IIC_SCL      PTAD_PTAD3              // change to match your GPIO
#define IIC_DD_SDA   PTADD_PTADD2            // change to match your GPIO
#define IIC_DD_SCL   PTADD_PTADD3            // change to match your GPIO
3. Global Constants
4. Global Variables
byte slave,reg,val,cmd;
word found;
5. Function Headers
//---------------------------------------------------------------------
byte IIC_Init(void);
word IIC_Find(byte LastAddress);
byte IIC_SlaveRegRead(byte SlaveAddress,byte Reg,byte *Result);
byte IIC_SlaveRegWrite(byte SlaveAddress,byte Reg,byte Val);
byte IIC_SlaveRegReadN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayIn);
byte IIC_SlaveRegWriteN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayOut);
6. Main
MCU_Init();
SCI_Init();
(void)IIC_Init();
slave=0x01;
for(;;)
  {
  cmd=SCI_CharIn();
  if (cmd=='f')
    {
    found=IIC_Find(0x7F);
    SCI_TextOut("Found "); SCI_ByteOut(found/256); 
    SCI_TextOut(" slaves. Last address = 0x"); SCI_ByteOut(found%256); SCI_CRLF();    
    }
  else if (cmd=='r')
    {
    SCI_TextOut("Read from slave 0x"); SCI_ByteOut(slave); SCI_TextOut(" - register 0x");
    reg=SCI_ByteIn(); SCI_ByteOut(reg);
    (void)IIC_SlaveRegRead(slave,reg,&val);
    SCI_TextOut("--->0x"); SCI_ByteOut(val); SCI_CRLF();
    }
  else if (cmd=='w')
    {
    SCI_TextOut("Write to  slave 0x"); SCI_ByteOut(slave); SCI_TextOut(" - register 0x");
    reg=SCI_ByteIn(); SCI_ByteOut(reg); 
    SCI_TextOut("<---0x");
    val=SCI_ByteIn(); SCI_ByteOut(val);
    (void)IIC_SlaveRegWrite(slave,reg,val); SCI_CRLF();
    }
  else if (cmd=='s')
    {
    SCI_TextOut("Set slave address to: 0x");
    slave=SCI_ByteIn(); SCI_ByteOut(slave); SCI_CRLF();
    }	
  else 
	  {
	  SCI_TextOut("r=Read, w=Write, s=Slave address, f=Find slaves."); SCI_CRLF();
	  }
  }
7. Function Bodies


// IIC FUNCTIONS (MASTER) /////////////////////////////////////////////
//---------------------------------------------------------------------
//.... Private Functions, hidden from main() ..........................
void Slow()      {byte burn; for (burn=0;burn<20;burn++);}
//.....................................................................
void SDA0()      {IIC_SDA=0; IIC_DD_SDA=1; Slow();}
//.....................................................................
void SDA1()      {           IIC_DD_SDA=0; Slow();}
//.....................................................................
void SCL0()      {IIC_SCL=0; IIC_DD_SCL=1; Slow();}
//.....................................................................
void SCL1()      {           IIC_DD_SCL=0; Slow();}
//.....................................................................
void Ack()       {SDA0(); SCL1(); SCL0();}
//.....................................................................
void Nak()       {SDA1(); SCL1(); SCL0();}
//.....................................................................
void IIC_Start() {SDA1(); SCL1(); SDA0(); SCL0();}
//.....................................................................
void IIC_Stop()  {SDA0(); SCL1(); SDA1();}
//.....................................................................
byte IIC_TxCycle(byte bout) 
{
byte i,p;
word timeout;
timeout=0;
p=0x80;
for (i=0;i<8;i++) 
  {
  if (p&bout) SDA1(); else SDA0();
  SCL1(); SCL0();
  p>>=1;
  }
SDA1();
SCL1();
Slow();
while ((IIC_SDA)&&(timeout<0x0100)) timeout++;
SCL0();
SDA1();
Slow();
if (timeout>=0x0100) return 1; else return 0;
}
//.....................................................................
byte IIC_RxCycle(byte ack) 
{
byte i,p,r;
p=0x80;
r=0;
SDA1();
Slow();
for (i=0;i<8;i++) 
  {
  SCL1();
  Slow();
  if (IIC_SDA) r+=p;
  SCL0();
  Slow();
  p>>=1;
  }
SDA1();
if (ack) Ack(); else Nak();
return r;
} 
//---- Public Functions, seen by main() -------------------------------
byte IIC_Init(void)
{
IIC_DD_SDA=0; IIC_DD_SCL=0; IIC_SDA=0; IIC_SCL=0; Slow();
if ((IIC_SDA)&&(IIC_SCL)) return 0; else return 1;
}
//---------------------------------------------------------------------
word IIC_Find(byte LastAddress)              // tries addresses from 0x00 through LastAddress (clamped to 0x7F)
{                                            // returns word; hi-byte indicates how many slaves were found; lo-byte shows address of last one found
byte found,try,slaveaddress,dummy;
(void)IIC_Init();
slaveaddress=0xFF; dummy=0; found=0;
for (try=0x00;try<=(LastAddress&0x7F);try++) if (IIC_SlaveRegRead(try,0,&dummy)==0) {slaveaddress=try; found++;}
return found<<8|slaveaddress;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegRead(byte SlaveAddress,byte Reg,byte *Result)
{
byte err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(Reg);
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1|1);
*Result=IIC_RxCycle(0);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegWrite(byte SlaveAddress,byte Reg,byte Val)
{
byte err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(Reg);
err|=IIC_TxCycle(Val);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegReadN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayIn)
{
byte i,err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(FirstReg);
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1|1);
for (i=0;i<Nbytes-1;i++) {*ArrayIn=IIC_RxCycle(1); ArrayIn++;}
*ArrayIn=IIC_RxCycle(0);
IIC_Stop();
return err;
}
//---------------------------------------------------------------------
byte IIC_SlaveRegWriteN(byte SlaveAddress,byte FirstReg,byte Nbytes,byte *ArrayOut)
{
byte i,err;
err=0;
IIC_Start();
err|=IIC_TxCycle(SlaveAddress<<1);
err|=IIC_TxCycle(FirstReg);
for (i=0;i<Nbytes;i++) {err|=IIC_TxCycle(*ArrayOut); ArrayOut++;}
IIC_Stop();
return err;
}
8. Interrupt Bodies
ENDMODULE


MODULE:A/D
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
for(;;)
  { 
  adcin=ADC_Read(Pot);
  if (adcin&0x80) LEDR=1; else LEDR=0;
  if (adcin&0x40) LEDG=1; else LEDG=0;
  if (adcin&0x20) LEDB=1; else LEDB=0;
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:ANALOG_COMP
1. Includes
2. Defines
#define ACMP_OUTPUT  ACMPSC_ACO 
3. Global Constants
4. Global Variables
byte flag,comparator;                        // flag indicates an interrupt occurred; comparator is the logic output of the analog comparator
5. Function Headers
6. Main
MCU_Init(); 
SPMSC1=0x01;                                 // enable bandgap reference voltage (1.2V) for ACMP+ input
MCU_Init(); 
ACMPSC=0xD3;                                 // enable comparator, bandgap select, interrupts enabled, compare output falling and rising edges
_asm CLI;                                    // enable MCU interrupts
flag=comparator=0;
for(;;)
  {
  if (flag)                                  // do nothing unless flag=1 (when comparator voltage >}
    {
    Buzzer(1); MCU_Delay(500);               // Buzzer and Blue LED ON for 50ms
    Buzzer(0); flag=0;                       // then back off
    if (comparator==1) RGB(1,0,0);
    else RGB(0,1,0);
    }
  }
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vacmp void ANALOG_COMPARATOR(void)
{
flag=1;                                     // notify main()
if (ACMP_OUTPUT) comparator=1; 
else comparator=0;
ACMPSC_ACF=1;                               // Clear ACF flag before exiting
}
ENDMODULE

MODULE:EXTCLOCK
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte internal;
5. Function Headers
6. Main
MCU_Init();                                  // running at 16MHz bus
internal=1;                                  // starting with internal clock (initialized in MCU_Init)
for (;;)
  {
  if (!nSW4)                                 // only switch to external (FBE) if SW4 pressed
    {  
    ICSC1=0x80;                              // 4MHz external Ref Clock selected, FLL bypassed
    ICSC2=0x24;                              // Bus speed (/1), Range 1, Ext Osc select 
    ICSSC&=~0x80;                            // 4MHz/2=2MHz (2.0MHz bus)
    }
  Buzzer(1); LEDR=1; MCU_Delay(300);         // Buzzer and Red LED ON
  Buzzer(0); LEDR=0; MCU_Delay(300);         // Buzzer and Red LED OFF
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE



MODULE:STOPMODE
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
IRQSC_IRQPE=1;                               // must have IRQ Pull-up enabled, otherwise MCU will exit STOP2 mode immediately
RGB(0,1,0);                                  // Green LED until SW3 is pressed
while (nSW4);                                // wait for SW3 to be pressed
PTAD =0x00;                                  // set all GPIO to a low or high state
PTBD =0x00;                                  // so they will not draw excessive current
PTCD =0x00;
PTADD=0xFF;                                  // Set all GPIO to an output state
PTBDD=0xFF;                                  // so they will not draw excessive current
PTCDD=0xFF;
RGB(1,0,0);                                  // Red LED during delay
MCU_Delay(30000);                            // delay 3 seconds before entering STOP2 mode
RGB(0,0,0);                                  // all LEDs off
_asm stop;                                   // STOP instruction, run directly in assembly
for (;;);                                    // Important!: BDM cable must be unplugged from demo board
                                             // or target board. Measured current=0.25uA
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:IRQPIN
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
SOPT1=0x22;                                  // enable RESET pin as an IRQ pin input
MCU_Init();                                  // only the first write to the SOPT1 is valid, hence the above line
_asm CLI;                                    // enable CPU interrupts
IRQSC=0x12;                                  // pull-up enable, falling edge sensitive, interrupt request (no polling), falling/rising edge only
RGB(0,1,0);                                  // turn on Green LED while we wait
for (;;);                                    // forever loop; ground IRQ pin (J12-48) to trigger interrupt 
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Virq void EXTERNAL_IRQ(void)
{
RGB(0,0,1);                                  // show interrupt by turning on Blue LED
IRQSC_IRQACK=1;                              // then clear IRQ flag before exiting
}
ENDMODULE


MODULE:RTC
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte flag;                                   // indicates that an RTC interrupt just occurred
5. Function Headers
6. Main
MCU_Init();                                  
flag=0;                                      // start with flag=0
_asm CLI;                                    // enable CPU interrupts
RTCSC=0x1F;                                  // enable RTC to be 1kHz internal / 1000 = 1s
for(;;)
  {
  if (flag)                                  // do nothing unless flag=1
    {
    Buzzer(1); LEDR=1; MCU_Delay(500);       // Buzzer and Red LED ON for 50ms
    Buzzer(0); LEDR=0; flag=0;               // then back off
    }
  }
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vrtc void REAL_TIME_COUNTER(void)
{
flag=1;                                      // change flag for main()
RTCSC_RTIF=1;                                // clear RTC flag before exiting
}
ENDMODULE


MODULE:KBI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // *** JUMPER WIRE NEEDED to connect J12-20 to J11-4 ***        
PTBSC=0x00;                                  // mask Port B Pin interrupts 
PTBPS=0x04;                                  // enable interrupt Port B pin 2, PIB2
PTBES=0x00;                                  // enable Port B pin PIB2 pull-up
PTBSC=0x03;                                  // unmask Port B interrupt request (no polling), edge only 
_asm CLI;                                    // enable CPU interrupts
PTBSC_PTBACK=1;                              // clear Port B interrupt flag
for (;;) {LEDG=0;}                           // forever loop
                   
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vportb void Keyboard_Interrupt(void)
{
LEDG=1;                                      // have Green LED reflect status of SW4 when interrupt occurs
PTBSC_PTBACK=1;                              // clear Port B interrupt flag
}
ENDMODULE



MODULE:TPM_PWM
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte mode;                                   // mode tracks the 2 PWM options
5. Function Headers
6. Main
MCU_Init();
mode=0;                                      // start in mode 1 (LEDs off, no buzzer)
for (;;)
  {
  if ((!nSW4)&&(mode!=2))                    // only enter mode 2 if SW4 pressed and not already in mode 2
    {
    TPM1SC=0x08;                             // busclk/1=62.5ns per count; buzzer on       
    TPM1C1SC=0x28;                           // edge-aligned PWM
    TPM1MOD=36363-1;                         // 440Hz
    TPM1C1V=36363>>1;                        // half of the above to produce 50% duty cycle PWM
    RGB(0,1,0); mode=2;                      // turn Green LED on, and remember that we are in mode 1
    }
  else if ((nSW4)&&(mode!=0))                // only enter mode 0 if neither SW4 pressed, and not already in mode 0
    {
    TPM1SC=0x00;                             // busclk/1=62.5ns per count; buzzer off
    RGB(0,0,0); mode=0;                      // turn LEDs off, and remember that we are in mode 0
    } 
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:FLASH_PGM uses SCI
1. Includes
2. Defines
3. Global Constants
// Table in FLASH that contains the assembly code that will run from RAM (starting at location 0x0084)
/* 
a000084 55 80                   ldhx   FLadr
a000086 B6 82                   lda    FLdta
a000088 F7                      sta    0,x
a000089 B6 83                   lda    FLcmd
a00008B C7 1826                 sta    FCMD
a00008E C6 1825                 lda    FSTAT
a000091 AA 80                   ora    #$80
a000093 C7 1825                 sta    FSTAT
a000096 9D                      nop
a000097 9D                      nop
a000098 9D                      nop
a000099 A4 7F                   and    #$7F
a00009B C7 1825                 sta    FSTAT
a00009E C6 1825     wait:       lda    FSTAT
a0000A1 A4 40                   and    #$40
a0000A3 27 F9                   beq    wait            
a0000A5 81                      rts             
*/
const byte PgmFlash[34]={                    // code to program Flash (will xfer to RAM)
0x55,0x80,0xB6,0x82,0xF7,0xB6,0x83,0xC7,0x18,0x26,0xC6,0x18,0x25,0xAA,
0x80,0xC7,0x18,0x25,0x9D,0x9D,0x9D,0xA4,0x7F,0xC7,0x18,0x25,0xC6,0x18,
0x25,0xA4,0x40,0x27,0xF9,0x81};
4. Global Variables
// Variables in RAM that must be filled in before calling the programming routine (make sure other variables do not overwrite these locations!)
word FLadr@0x0080;                           // these variables are mapped to RAM
byte FLdta@0x0082;                           // so Flash programming can use them
byte FLcmd@0x0083;                           // as parameters
byte PgmRAM[34]@0x0084;                      // this table will contain program to run from RAM
byte cmd,adrh,adrl,val;
byte *mem;
word adr16;
5. Function Headers
//---------------------------------------------------------------------
void Flash_BlockErase(word adr);
void Flash_ByteWrite(word adr,byte val);
6. Main
MCU_Init();
SCI_Init();
// Flash Init
FCDIV=0x28;                                  // 8MHz/(39+1)=200kHz; must fall close to 200kHz
for(;;)
  {
  cmd=SCI_CharIn();
  if (cmd=='r')
    {
    SCI_TextOut("Read from      address 0x");
    adrh=SCI_ByteIn(); adrl=SCI_ByteIn(); SCI_ByteOut(adrh); SCI_ByteOut(adrl);
    mem=(byte *)(adrh*256+adrl);
    SCI_TextOut("--->0x");
    SCI_ByteOut(*mem);
    SCI_CRLF();
    }
  else if (cmd=='w')
    {
    SCI_TextOut("Write to       address 0x");
    adrh=SCI_ByteIn(); adrl=SCI_ByteIn(); SCI_ByteOut(adrh); SCI_ByteOut(adrl); 
    adr16=adrh*256+adrl;
    SCI_TextOut("<---0x");
    val=SCI_ByteIn(); SCI_ByteOut(val);
    Flash_ByteWrite(adr16,val);
    SCI_CRLF();
    }
  else if (cmd=='e')
    {
    SCI_TextOut("Erase Block at address 0x");
    adrh=SCI_ByteIn(); adrl=SCI_ByteIn(); SCI_ByteOut(adrh); SCI_ByteOut(adrl); 
    adr16=adrh*256+adrl;
    Flash_BlockErase(adr16);
    SCI_CRLF();
    }	
  else 
	  {
	  SCI_TextOut("r=Read, w=Write, e=Erase Block. Reads from bad locations will cause a reset!");
	  SCI_CRLF();
	  }
  }
7. Function Bodies


// Flash FUNCTIONS ////////////////////////////////////////////////////
//---------------------------------------------------------------------
void Flash_BlockErase(word adr)
{
byte i;
for (i=0;i<34;i++) PgmRAM[i]=PgmFlash[i];                 // copy Flash programmer to RAM; need to run code from RAM
FLadr=adr; FLdta=0x00; FLcmd=0x40; _asm jsr $0084;        // erase 512-byte page(for only 3 bytes!!!)
}
//---------------------------------------------------------------------
void Flash_ByteWrite(word adr,byte val)
{
byte i;
for (i=0;i<34;i++) PgmRAM[i]=PgmFlash[i];                 // copy Flash programmer to RAM; need to run code from RAM
FLadr=adr; FLdta=val; FLcmd=0x20; _asm jsr $0084;         // erase 512-byte page(for only 3 bytes!!!)
}
8. Interrupt Bodies
ENDMODULE


MODULE:BOARD_TEST
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
for (;;)                                     // Factory Board Test
  {
  LEDR^=1;                                   // toggle Red LED
  if (!nSW4)                                 // in SW4 pressed,
    {
    Buzzer(1);                               // sound buzzer
    while (!nSW4);
    Buzzer(0);
    }
  adcin=ADC_Read(Pot);
  MCU_Delay(adcin*10);                       // delay variable with pot position
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


PRMSTART
/* This is a linker parameter file for the mc9s08sh16 */

NAMES END /* CodeWarrior will pass all the needed files to the linker by command line. But here you may add your own files too. */

SEGMENTS /* Here all RAM/ROM areas of the device are listed. Used in PLACEMENT below. */
    Z_RAM                    =  READ_WRITE   0x0080 TO 0x00FF;
    RAM                      =  READ_WRITE   0x0100 TO 0x047F;
    ROM                      =  READ_ONLY    0xC000 TO 0xFFAD;
 /* INTVECTS                 =  READ_ONLY    0xFFC0 TO 0xFFFF; Reserved for Interrupt Vectors */
END

PLACEMENT /* Here all predefined and user segments are placed into the SEGMENTS defined above. */
    DEFAULT_RAM,                        /* non-zero page variables */
                                        INTO  RAM;

    _PRESTART,                          /* startup code */
    STARTUP,                            /* startup data structures */
    ROM_VAR,                            /* constant variables */
    STRINGS,                            /* string literals */
    VIRTUAL_TABLE_SEGMENT,              /* C++ virtual table segment */
    DEFAULT_ROM,
    COPY                                /* copy down information: how to initialize variables */
                                        INTO  ROM; 

    _DATA_ZEROPAGE,                     /* zero page variables */
    MY_ZEROPAGE                         INTO  Z_RAM;
END

STACKSIZE 0x80

VECTOR 0 _Startup /* Reset vector: this is the default entry point for an application. */

PRMEND
