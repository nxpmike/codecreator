Revision=4
MenuName=S08_SH4
MenuComment=8-bit/S08 Core/4KB Flash/128B RAM/
IncludeName=<MC9S08SH4.h>
MCUID=0x014
CURRENTDEVICE=9S08SH4


MODULE:INIT
1. Includes
#include "derivative.h"                      // gives acces to all MCU registers
2. Defines
#define Overflow2    TPM2SC_TOF              // used by MCU_Delay()
#define LEDR         PTAD_PTAD2              // Red   LED, positive logic (1=on, 0=off)
#define nSW4         PTAD_PTAD3              // Switch 4,  negative logic (0=pressed, 1=not pressed)
#define Pot          0x01                    // Potentiometer is on A/D channel 0x01
3. Global Constants
4. Global Variables
byte R,adcin;                                // R is an LED flag; adcin will store ADC results
5. Function Headers
void MCU_Init(void);                         // initializes MCU for the Universal S08 Tower Board
void MCU_Delay(word delay);                  // delay in multiples of 100us
//---------------------------------------------------------------------
void Buzzer(byte OnOff);                     // Buzzer Control:  1=on, 0=off
byte ADC_Read(byte chan);                    // Analog-to-Digital Converter byte read: enter channel to read
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
R=0;                                         // start with LED off
for (;;)                                     // forever loop
  {
  R^=1;                                      // toggle Red LED unless...
  if (!nSW4) R=0;                            // pressing SW4 turns Green LED on while pressed
  Buzzer(R);                                 // toggle Buzzer if SW4 was not pressed  
  adcin=ADC_Read(Pot);                       // take an 8-bit A/D reading of the Pot
  TPMMOD=adcin<<8;                           // shift Pot value left 8 bits, to create 16-bit timer period value (modulo) 
  TPMC0V=adcin<<7;                           // shift Pot value left 7-bits, to create half of the above: 50% duty cycle PWM
  if (R==1) LEDR=1; else LEDR=0;             // act on Red LED based on variable R
  MCU_Delay(5000);                           // run a 5000 * 100us = 0.5s delay
  }
7. Function Bodies
void MCU_Init(void) 
{
// Crucial            
_asm SEI;                                    // disable interrupts
SOPT1=0x23;                                  // disable COP, enable Stop mode; Reset & Bkgd = BDM
SPMSC1=0x00;                                 // disable LVDSE
SPMSC2=0x01;                                 // enable Stop2 Mode
// System Clock Init
ICSTRM=NVICSTRM;                             // initialize ICSTRM register from NVICSTRM
ICSSC =NVFTRIM;                              // initialize ICSSC  register from NVFTRIM
ICSC1=0x04;
ICSC2=0x00;                                  // go to full bus speed /1, instead of /2 (default); Trim*512=31250Hz*512=16.000MHz (8.000MHz bus)
// ADC Init
ADCCFG=0x00;                                 // 8 bits & 8MHz clock (/1)
// Timer1 Init (Buzzer)
TPM1SC=0x00;                                 // busclk/1=125ns per count; buzzer on=0x08, off=0x00
TPM1C0SC=0x28;                               // edge-aligned PWM
TPM1MOD=18182-1;                             // 440Hz by default
TPM1C0V=18182>>1;                            // half of the above to produce 50% duty cycle PWM
// Timer2 Init
TPM2SC=0x08;                                 // busclk/1=125ns per count
TPM2MOD=800-1;                               // 800*125ns=100us period
// GPIO Init
PTAD=0x00;                                   // LEDR (PTA2) off by default                                   
PTADD=0x04;                                  // LEDR (PTA2) is an output
PTAPE=0x08;                                  // enable pull-ups for nSW4 (PTA3)
}
//---------------------------------------------------------------------
void MCU_Delay(word delay)                   // Delay in multiples of 100us (e.g. use 10000 for 1 second)
{word delw; for (delw=0;delw<delay;delw++) {while (!Overflow2); Overflow2=0;}}
//---------------------------------------------------------------------
void Buzzer(byte OnOff)                      // Buzzer Control:  1=on, 0=off
{if (OnOff==1) TPM1SC=0x08; else TPM1SC=0x00;}
//---------------------------------------------------------------------
byte ADC_Read(byte chan)                     // Analog-to-Digital Converter byte read: enter channel to read
{ADCSC1=chan; while (!ADCSC1_COCO); return ADCRL;}
8. Interrupt Bodies
ENDMODULE


MODULE:A/D
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
for(;;)
  {
  adcin=ADC_Read(Pot);                       // read ADC for POT
  MCU_Delay(adcin*10);                       // delay variable with pot position
  LEDR^=1;                                   // flash RED LED in reference to POT position
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:IRQPIN
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
SOPT1=0x22;                                  // enable RESET pin as an IRQ pin input
MCU_Init();                                  // only the first write to the SOPT1 is valid, hence the above line
_asm CLI;                                    // enable CPU interrupts
IRQSC=0x12;                                  // pull-up enable, falling edge sensitive, interrupt request (no polling), falling/rising edge only
LEDR=1;                                      // turn on RED LED while we wait
for (;;);                                    // forever loop; ground IRQ pin (J12-48) to trigger interrupt 
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Virq void EXTERNAL_IRQ(void)
{
LEDR=0;                                      // show interrupt by turning off RED LED
IRQSC_IRQACK=1;                              // then clear IRQ flag before exiting
}
ENDMODULE


MODULE:RTC
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte flag;                                   // indicates that an RTC interrupt just occurred
5. Function Headers
6. Main
MCU_Init();                                  
flag=0;                                      // start with flag=0
_asm CLI;                                    // enable CPU interrupts
RTCSC=0x1F;                                  // enable RTC to be 1kHz internal / 1000 = 1s
for(;;)
  {
  if (flag)                                  // do nothing unless flag=1
    {
    LEDR=1; MCU_Delay(500);                  // Buzzer and Red LED ON for 50ms
    LEDR=0; flag=0;                          // then back off
    }
  }
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vrtc void REAL_TIME_COUNTER(void)
{
flag=1;                                      // change flag for main()
RTCSC_RTIF=1;                                // clear RTC flag before exiting
}
ENDMODULE


MODULE:STOPMODE
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();
IRQSC_IRQPE=1;                               // must have IRQ Pull-up enabled, otherwise MCU will exit STOP2 mode immediately
LEDR=1;                                      // RED LED ON until SW3 is pressed
while (nSW4);                                // wait for SW3 to be pressed
PTAD =0x00;                                  // set all GPIO to a low or high state
PTADD=0xFF;                                  // Set all GPIO to an output state
MCU_Delay(30000);                            // delay 3 seconds before entering STOP2 mode
LEDR=0;                                      // RED LED OFF
_asm stop;                                   // STOP instruction, run directly in assembly
for (;;);                                    // Important!: BDM cable must be unplugged from demo board
                                             // or target board. Measured current=0.25uA
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:KBI
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // *** JUMPER WIRE NEEDED to connect J12-20 to J11-4 ***        
PTASC=0x00;                                  // mask Port B Pin interrupts 
PTAPS=0x08;                                  // enable interrupt Port A pin 3, PIA3
PTAES=0x00;                                  // enable Port A pin PIA3 pull-up
PTASC=0x03;                                  // unmask Port B interrupt request (no polling), edge only 
_asm CLI;                                    // enable CPU interrupts
PTASC_PTAACK=1;                              // clear Port B interrupt flag
for (;;) {LEDG=0;}                           // forever loop
                   
7. Function Bodies
8. Interrupt Bodies
interrupt VectorNumber_Vportb void Keyboard_Interrupt(void)
{
LEDG=1;                                      // have Green LED reflect status of SW4 when interrupt occurs
PTASC_PTAACK=1;                              // clear Port B interrupt flag
}
ENDMODULE


MODULE:TPM_PWM
1. Includes
2. Defines
3. Global Constants
4. Global Variables
byte mode;                                   // mode tracks the 2 PWM options
5. Function Headers
6. Main
MCU_Init();
mode=0;                                      // start in mode 0 (LEDs off, no buzzer)
for (;;)
  {
  if ((!nSW4)&&(mode!=2))                    // only enter mode 2 if SW4 pressed and not already in mode 2
    {
    TPM1SC=0x08;                             // busclk/1=125ns per count; buzzer on       
    TPM1C0SC=0x28;                           // edge-aligned PWM
    TPM1MOD=18182-1;                         // 440Hz
    TPM1C0V=18182>>1;                        // half of the above to produce 50% duty cycle PWM
    LEDR=1; mode=2;                          // turn RED LED on, and remember that we are in mode 2
    }
  else if ((nSW4)&&(mode!=0))                // only enter mode 0 if neither SW4 pressed, and not already in mode 0
    {
    TPM1SC=0x00;                             // busclk/1=125ns per count; buzzer off
    LEDR=0; mode=0;                          // turn LEDs off, and remember that we are in mode 0
    } 
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


MODULE:FLASH_PGM
1. Includes
2. Defines
3. Global Constants
// Table in FLASH that contains the assembly code that will run from RAM (starting at location 0x0084)
/* 
a000084 55 80                   ldhx   FLadr
a000086 B6 82                   lda    FLdta
a000088 F7                      sta    0,x
a000089 B6 83                   lda    FLcmd
a00008B C7 1826                 sta    FCMD
a00008E C6 1825                 lda    FSTAT
a000091 AA 80                   ora    #$80
a000093 C7 1825                 sta    FSTAT
a000096 9D                      nop
a000097 9D                      nop
a000098 9D                      nop
a000099 A4 7F                   and    #$7F
a00009B C7 1825                 sta    FSTAT
a00009E C6 1825     wait:       lda    FSTAT
a0000A1 A4 40                   and    #$40
a0000A3 27 F9                   beq    wait            
a0000A5 81                      rts             
*/
const byte PgmFlash[34]={                    // code to program Flash (will xfer to RAM)
0x55,0x80,0xB6,0x82,0xF7,0xB6,0x83,0xC7,0x18,0x26,0xC6,0x18,0x25,0xAA,
0x80,0xC7,0x18,0x25,0x9D,0x9D,0x9D,0xA4,0x7F,0xC7,0x18,0x25,0xC6,0x18,
0x25,0xA4,0x40,0x27,0xF9,0x81};
4. Global Variables
// Variables in RAM that must be filled in before calling the programming routine (make sure other variables do not overwrite these locations!)
word FLadr@0x0080;                           // these variables are mapped to RAM
byte FLdta@0x0082;                           // so Flash programming can use them
byte FLcmd@0x0083;                           // as parameters
byte PgmRAM[34]@0x0084;                      // this table will contain program to run from RAM
byte cmd,adrh,adrl,val;
byte *mem;
word adr16;
5. Function Headers
//---------------------------------------------------------------------
void Flash_BlockErase(word adr);
void Flash_ByteWrite(word adr,byte val);
6. Main
MCU_Init();
// Flash Init
FCDIV=0x28;                                  // 8MHz/(40)=200kHz; must fall close to 200kHz
Flash_BlockErase(0xF200);                    // Erases flash sector F200
Flash_ByteWrite(0xF200,0x77);                // Writes data 0x77 at address location 0xF200
for(;;);

7. Function Bodies
// Flash FUNCTIONS ////////////////////////////////////////////////////
//---------------------------------------------------------------------
void Flash_BlockErase(word adr)
{
byte i;
for (i=0;i<34;i++) PgmRAM[i]=PgmFlash[i];                 // copy Flash programmer to RAM; need to run code from RAM
FLadr=adr; FLdta=0x00; FLcmd=0x40; _asm jsr $0084;        // erase 512-byte page(for only 3 bytes!!!)
}
//---------------------------------------------------------------------
void Flash_ByteWrite(word adr,byte val)
{
byte i;
for (i=0;i<34;i++) PgmRAM[i]=PgmFlash[i];                 // copy Flash programmer to RAM; need to run code from RAM
FLadr=adr; FLdta=val; FLcmd=0x20; _asm jsr $0084;         // erase 512-byte page(for only 3 bytes!!!)
}
8. Interrupt Bodies
9. NO SCI support for this module
ENDMODULE




MODULE:BOARD_TEST
1. Includes
2. Defines
3. Global Constants
4. Global Variables
5. Function Headers
6. Main
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
for (;;)                                     // Factory Board Test
  {
  LEDR^=1;                                   // toggle Red LED
  if (!nSW4)                                 // in SW4 pressed,
    {
    Buzzer(1);                               // sound buzzer
    while (!nSW4);
    Buzzer(0);
    }
  adcin=ADC_Read(Pot);
  MCU_Delay(adcin*10);                       // delay variable with pot position
  }
7. Function Bodies
8. Interrupt Bodies
ENDMODULE


PRMSTART
/* This is a linker parameter file for the mc9s08sh4 */

NAMES END /* CodeWarrior will pass all the needed files to the linker by command line. But here you may add your own files too. */

SEGMENTS /* Here all RAM/ROM areas of the device are listed. Used in PLACEMENT below. */
    Z_RAM                    =  READ_WRITE   0x0080 TO 0x00FF;
    RAM                      =  READ_WRITE   0x0100 TO 0x017F;
    ROM                      =  READ_ONLY    0xF000 TO 0xFFAD;
 /* INTVECTS                 =  READ_ONLY    0xFFC0 TO 0xFFFF; Reserved for Interrupt Vectors */
END

PLACEMENT /* Here all predefined and user segments are placed into the SEGMENTS defined above. */
    DEFAULT_RAM,                        /* non-zero page variables */
                                        INTO  RAM;

    _PRESTART,                          /* startup code */
    STARTUP,                            /* startup data structures */
    ROM_VAR,                            /* constant variables */
    STRINGS,                            /* string literals */
    VIRTUAL_TABLE_SEGMENT,              /* C++ virtual table segment */
    DEFAULT_ROM,
    COPY                                /* copy down information: how to initialize variables */
                                        INTO  ROM; 

    _DATA_ZEROPAGE,                     /* zero page variables */
    MY_ZEROPAGE                         INTO  Z_RAM;
END

STACKSIZE 0x30

VECTOR 0 _Startup /* Reset vector: this is the default entry point for an application. */

PRMEND
